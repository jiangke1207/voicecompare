//
//  Defines.h
//  zhangshangxiaoma
//
//  Created by jiangqc on 14-9-1.
//  Copyright (c) 2014年 jiangqc. All rights reserved.
//

#ifndef zhangshangxiaoma_Defines_h
#define zhangshangxiaoma_Defines_h

#define kScreen_Height [UIScreen mainScreen].bounds.size.height
#define kScreen_Width [UIScreen mainScreen].bounds.size.width

#define SYSTEM_IS_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define SCREEN_IS_4INCH ([[UIScreen mainScreen] bounds].size.height > 480.0)

#define RESOURCE_OF_PATH(X,Y) [[NSBundle mainBundle] pathForResource:(X) ofType:(Y)]
#define IMAGE_OF_PATH_X2(N) [UIImage imageWithContentsOfFile:RESOURCE_OF_PATH(([NSString stringWithFormat:@"%@@2x", (N)]), @"png")]
#define IMAGE_OF_PATH_X1(N) [UIImage imageWithContentsOfFile:RESOURCE_OF_PATH(([NSString stringWithFormat:@"%@", (N)]), @"png")]

#define DOCUMENT_OF_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define RGBACG(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a].CGColor

#endif

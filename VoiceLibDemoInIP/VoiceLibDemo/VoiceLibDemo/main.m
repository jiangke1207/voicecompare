//
//  main.m
//  VoiceLibDemo
//
//  Created by 姜珂 on 14/10/29.
//  Copyright (c) 2014年 Xiaoma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

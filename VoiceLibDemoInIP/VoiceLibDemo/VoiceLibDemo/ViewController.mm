//
//  ViewController.m
//  AudioDemo
//
//  Created by jiangqc on 14/10/29.
//  Copyright (c) 2014年 jiangqc. All rights reserved.
//

#import "ViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "MainEntry.h"
#import "Defaults.h"
#import "Defines.h"
#import "UIViewExt.h"

@interface ViewController () <AVAudioRecorderDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    AVAudioRecorder *_audioRecorder;
    AVAudioPlayer *_audioPlay;
    
    UIPickerView *_pickerView;
    
    NSMutableDictionary *_recordSetting;
    NSMutableArray *_pickerNameArray;
    
    NSInteger _audioNumber;
    NSInteger _selectPickerNum;
}

@property (weak, nonatomic) IBOutlet UIButton *selectAudioBtn;

@property (weak, nonatomic) IBOutlet UITextView *showOriginalTextView;

@property (weak, nonatomic) IBOutlet UITextView *showResultTextView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initData];
    [self initView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)initData
{
    NSError *error = nil;
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    [session setActive:YES error:&error];
    
    _recordSetting = [[NSMutableDictionary alloc] init];
    [_recordSetting setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
    [_recordSetting setValue:[NSNumber numberWithFloat:8000] forKey:AVSampleRateKey];
    [_recordSetting setValue:[NSNumber numberWithInt:1] forKey:AVNumberOfChannelsKey];
    [_recordSetting setValue:[NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];
    [_recordSetting setValue:[NSNumber numberWithInt:AVAudioQualityHigh] forKey:AVEncoderAudioQualityKey];
    
    _audioNumber = 0;
    _pickerNameArray = [NSMutableArray arrayWithObjects:@"test01", @"test02", @"test03", nil];
    
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL existed = [fileManager fileExistsAtPath:[NSString stringWithFormat:@"%@/TestAudio/",DOCUMENT_OF_PATH] isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) )
    {
        [fileManager createDirectoryAtPath:[NSString stringWithFormat:@"%@/TestAudio/",DOCUMENT_OF_PATH] withIntermediateDirectories:YES attributes:nil error:nil];
    }
}

- (void)initView
{
}

- (IBAction)selectAudioBtnAction:(UIButton *)sender {
    _selectPickerNum = 0;
    
    _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.height - 216, self.view.width, 216)];
    [_pickerView setBackgroundColor:[UIColor grayColor]];
    _pickerView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth; //这里设置了就可以自定义高度了，一般默认是无法修改其216像素的高度
    
    _pickerView.dataSource = self;
    _pickerView.delegate = self;
    _pickerView.showsSelectionIndicator = YES;
    
    [self.view addSubview:_pickerView];
    
    UIButton *okBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.view.width - 100, _pickerView.top + 10, 80, 30)];
    [okBtn setBackgroundColor:[UIColor blueColor]];
    [okBtn setTitle:@"确定" forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(okBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:okBtn];
}

- (void)okBtnAction:(UIButton *)sender
{
    [_pickerView removeFromSuperview];
    
    [sender removeFromSuperview];
    
    NSString *audioFileName = [_pickerNameArray objectAtIndex:_selectPickerNum];
    NSLog(@"you select %@", audioFileName);
    
    NSString *soundsPath = RESOURCE_OF_PATH(audioFileName, @"wavx");
    NSLog(@"soundsPath = %@", soundsPath);
    
    NSData *data = [NSData dataWithContentsOfFile:soundsPath];
    Byte *dataByte = (Byte *)data.bytes;
    int datalen = (int)data.length;
    FormatFile formatfile = GetWavFile(dataByte, datalen);
    
    int a=0;
}

- (IBAction)recordBtnTouchDown:(UIButton *)sender {
    NSError *error = nil;
    _audioNumber ++;
    
    NSString *audioPath = [NSString stringWithFormat:@"%@/TestAudio/num-%02li.wav",DOCUMENT_OF_PATH, (long)_audioNumber];
    NSLog(@"audioPath = %@",audioPath);
    
    _audioRecorder = [[AVAudioRecorder alloc] initWithURL:[NSURL URLWithString:audioPath] settings:_recordSetting error:&error];
    _audioRecorder.delegate = self;
    
    if ([_audioRecorder prepareToRecord]) {
        [_audioRecorder record];
    }
}

- (IBAction)recordBtnTouchDragExit:(UIButton *)sender {
    [_audioRecorder deleteRecording];
    [_audioRecorder stop];
    
    _audioNumber --;
}

- (IBAction)recordBtnTouchUpInside:(UIButton *)sender {
    double cTime = _audioRecorder.currentTime;
    if (cTime < 1) {
        [_audioRecorder deleteRecording];
        _audioNumber --;
    }
    
    [_audioRecorder stop];
}

- (IBAction)playRecordAudioBtnAction:(UIButton *)sender {
    if (_audioPlay.playing) {
        [_audioPlay stop];
        return;
    }
    
    NSString *audioPath = [NSString stringWithFormat:@"%@/TestAudio/num-%02li.wav",DOCUMENT_OF_PATH, (long)_audioNumber];
    NSLog(@"audioPath = %@",audioPath);
    
    _audioPlay = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL URLWithString:audioPath] error:nil];
    [_audioPlay play];
}

- (IBAction)playSoundsBtnAction:(UIButton *)sender {
    if (_audioPlay.playing) {
        [_audioPlay stop];
        return;
    }
    
    _audioPlay = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@TestAudio/num-%02li",DOCUMENT_OF_PATH, (long)_audioNumber]] error:nil];
    [_audioPlay play];
}









#pragma mark UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [_pickerNameArray count];
}

#pragma mark -
#pragma mark UIPickerViewDelegate
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *pickerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, self.view.width, 30)];
    [pickerLabel setBackgroundColor:[UIColor clearColor]];
    [pickerLabel setText:[_pickerNameArray objectAtIndex:row]];
    [pickerLabel setTextAlignment:NSTextAlignmentCenter];
    [pickerLabel setFont:[UIFont systemFontOfSize:15]];
    return pickerLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _selectPickerNum = row;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 40.0;
}


@end

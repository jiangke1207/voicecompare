//
//  ListFunctions.cpp
//  VoiceLibForMac
//
//  Created by 姜珂 on 14/10/28.
//  Copyright (c) 2014年 Xiaoma. All rights reserved.
//

#include <stdio.h>
#include "ListFunctions.h"

List<double> UnitaryList(List<double> inputlist)
{
    double max=0;
    for(int i=0; i< inputlist.Count(); i++)
    {
        if((inputlist[i]>=0 && inputlist[i]>max)||(inputlist[i]<0 && -inputlist[i]>max))
        {
            max=inputlist[i];
            if(max<0)
                max=-max;
        }
    }
    
    for(int i=0;i<inputlist.Count();i++)
    {
        
        inputlist[i]=inputlist[i]/max;
    }
    
    return inputlist;
}

VectorList<double> UnitaryVectorList(VectorList<double> input)
{
    double max=100;
    for( int i=0; i< input.Heigth; i++)
    {
        for(int j=0;j< input.Width;j++)
        {
            
            if(input.Data[i][j]>100)
                input.Data[i][j]=100;
            input.Data[i][j]=input.Data[i][j]/max;
        }
    }
    
    return input;
}

double Max(VectorList<double> inputlist)
{
    double maxvalue=inputlist.Data[0][0];
    for(int i=0;i< inputlist.Heigth;i++)
    {
        for(int j=0;j<inputlist.Width;j++)
        {
            if(inputlist.Data[i][j]>maxvalue)
                maxvalue=inputlist.Data[i][j];
        }
    }
    
    return maxvalue;
}

double Sum(List<double> input)
{
    double sum=0;
    for(int i=0;i<input.Count();i++)
    {
        sum+=input[i];
    }
    return sum;
}

VectorList<double> VectorTimes(VectorList<double> a, VectorList<double> b)
{
    VectorList<double> result(a.Heigth,b.Width,0);
    
    for(int i=0; i< a.Heigth; i++)
    {
        for (int j=0; j<b.Width;j++)
        {
            for(int q=0; q<a.Width; q++)
            {
                result.Data[i][j]+=a.Data[i][q]* b.Data[q][j];
                //printf("%d,%d-> a:%d,%d (%f) * b:%d,%d (%f) \n",i+1,j+1,i+1,q+1,a.Data[i][q],q+1,j+1,b.Data[q][j]);
            }
        }
    }
    
    return result;
}

double Sum(VectorList<double> input)
{
    double result=0;
    for( int i=0; i<input.Heigth; i++)
    {
        for( int j=0; j<input.Width; j++)
        {
            result+=input.Data[i][j];
        }
    }
    return result;
}

double SumColumn(VectorList<double> input,int Cindex)
{
    double result=0;
    for( int i=0; i<input.Heigth; i++)
    {
        
        result+=input.Data[i][Cindex];
        
    }
    return result;
}

double Dot( List<double> a, List<double> b)
{
    double r=0;
    for( int i=0; i< a.Count(); i++)
        r+=a[i]*b[i];
    return r;
}

int sign(double x)
{
    if(x>0)
        return 1;
    else
        return -1;
    
}

List<HillUnit> FindHills(List<double> input)
{
    List<HillUnit> result;
    
    int last=sign(input[1]-input[0]);
    int index=0;
    for(int i=2; i<input.Count();i++)
    {
        int f=sign(input[i]-input[i-1]);
        if(last!=f)
        {
            HillUnit hu;
            hu.start=index;
            hu.end=i-1;
            hu.tempforXielv=last;
            
            result.Add(hu);
            
            index=i;
            last=f;
        }
        
        if(i==input.Count()-1)
        {
            HillUnit hu;
            hu.start=index;
            hu.end=i-1;
            hu.tempforXielv=last;
            
            result.Add(hu);
        }
    }
    
    index=1;
    while(true)
    {
        if(index>=result.Count())
            break;
        if(result[index].tempforXielv+result[index-1].tempforXielv==0 && result[index].tempforXielv==-1 )
        {
            //int t=result[index].tempforXielv;
            result[index-1].center=result[index-1].end;
            result[index-1].centerValue=input[result[index-1].center];
            result[index-1].end=result[index].end;
            result[index-1].tempforXielv=0;
            result.Remove(index);
        }
        
        index++;
    }
    
    return result;
}

//
//  OFunctions.cpp
//  VoiceLibForMac
//
//  Created by 姜珂 on 14/10/28.
//  Copyright (c) 2014年 Xiaoma. All rights reserved.
//

#include <stdio.h>
#include "OFunctions.h"

///预加重
List<double> PreEmphasis(List<double> input)
{
    double preEmphasisFactor=0.98;
    
    List<double> Result;
    
    Result.Add(input[0]- preEmphasisFactor*input[0]);
    
    for(int i=1; i< input.Count(); i++)
    {
        Result.Add(input[i]-preEmphasisFactor* input[i-1]);
    }
    
    return Result;
}

///汉明窗
List<double> GetHamming(int len)
{
    List<double> hamming;
    
    for(int i=0; i<len; i++)
    {
        hamming.Add(0.54 -0.46* cos(2*pi*i/(len-1)));
    }
    
    return hamming;
}

///add window
VectorList<double> AddWindow(List<double> input, int windowsLen)
{
    int len=input.Count();
    
    int windowsCount=floor(len / (windowsLen/2))+1;
    
    List<double> windowFunction= GetHamming(windowsLen);
    
    VectorList<double> result(windowsCount,windowsLen,0);
    
    for(int ii=0; ii<windowsCount; ii++)
    {
        int sp=ii*(windowsLen/2);
        for(int jj=0; jj<windowsLen; jj++)
        {
            if(sp+jj> len)
            {
                break;
            }
            
            result.Data[ii][jj]=input[jj+sp]* windowFunction[jj];
        }
    }
    
    return result;
}

///FFT
List<COMPLEX> FFT(List<double> input,int inputlength)
{
    int level=floor(log2(inputlength));
    
    if (pow(2, level)< inputlength)
        level++;
    
    int colcount=pow(2, level);
    
    List<COMPLEX> result;
    
    for (int i=0; i< inputlength; i++)
    {
        COMPLEX nn;
        nn.real=input[i];
        nn.image=0;
        result.Add(nn);
    }
    
    COMPLEX u,w,t;
    
    int n , i , nv2 , j , k , le , l , le1 , ip , nm1 ;
    
    n = 1;
    for(i=0; i<level; i++)
        n = n*2 ;
    
    nv2 = n / 2 ;
    nm1 = n - 1 ;
    j = 1 ;
    
    for (i = 1 ; i <= nm1 ; i ++)
    {
        if (i < j)
        {
            t.real = result[i - 1].real ;
            t.image = result[i - 1].image ;
            result[i - 1].real = result[j - 1].real ;
            result[i - 1].image = result[j - 1].image ;
            result[j - 1].real = t.real ;
            result[j - 1].image = t.image ;
        }
        
        k = nv2 ;
        
        while (k < j)
        {
            j -= k ;
            k /= 2 ;
        }
        j += k ;
    }
    
    le = 1 ;
    for (l= 1 ; l <= level ; l ++)
    {
        le =le * 2 ;
        le1 = le / 2 ;
        u.real = 1.0f ;
        u.image = 0.0f ;
        w.real =  cos(pi / le1) ;
        w.image = -sin(pi / le1) ;
        
        for (j = 1 ; j <= le1 ; j ++)
        {
            i=j;
            for (int it = 1 ; it <= 10000 ; it ++)
            {
                i=j+(it-1)*le;
                if(i>n)break;
                ip = i + le1 ;
                t.real = result[ip - 1].real * u.real - result[ip - 1].image * u.image ;
                t.image = result[ip - 1].real * u.image + result[ip - 1].image * u.real ;
                result[ip - 1].real = result[i - 1].real - t.real ;
                result[ip - 1].image = result[i - 1].image - t.image ;
                result[i - 1].real = t.real + result[i - 1].real ;
                result[i - 1].image = t.image + result[i - 1].image ;
            }
            
            t.real = u.real * w.real - u.image * w.image ;
            t.image = u.image * w.real + u.real * w.image ;
            u.real = t.real ;
            u.image = t.image ;
        }
    }
    
    return result;
}

///FFT for every frame, freqlist is output for Frequency of every number
VectorList<COMPLEX> FFTWindowfulArray(VectorList<double> windowfulinput,int fs, List<double> &freqlist)
{
    int windowCount=windowfulinput.Heigth;
    int windowLen=windowfulinput.Width;
    
    COMPLEX defaut;
    defaut.real=0;
    defaut.image=0;
    
    VectorList<COMPLEX> result(windowCount,windowLen, defaut);
    
    
    for(int i=0; i< windowCount; i++)
    {
        List<COMPLEX> temp=FFT(windowfulinput.Data[i], windowLen);
        
        result.Data[i]=temp;
    }
    
    
    double freqStep=fs/(double)windowLen;
    
    for(int j=0; j< windowLen/2+1; j++)
    {
        freqlist.Add(freqStep* j);
    }
    
    return result;
}

///Power sum in every frame
List<double> PowerPerFrame(VectorList<double> windowsfulinput)
{
    List<double> result;
    
    for(int i=0; i<windowsfulinput.Heigth; i++)
    {
        result.Add(0);
        for(int j=0; j<windowsfulinput.Width; j++)
        {
            result[i]+= windowsfulinput.Data[i][j]*windowsfulinput.Data[i][j];
        }
    }
    
    //result=UnitaryList(result);
    return result;
}

///Get Entropy
List<double> GetEntropy(VectorList<COMPLEX> fftfulinput,int FS)
{
    int windowLength=fftfulinput.Width;
    int windowCount=fftfulinput.Heigth;
    
    List<double> result;
    
    double freqStep=FS/(double)windowLength;
    
    for(int i=0; i<windowCount; i++)
    {
        result.Add(0);
        List<COMPLEX> fftcurrent= fftfulinput.Data[i];
        double sumP=0;
        double* BP=new double[EntropypartCount];
        
        for(int j=0;j<EntropypartCount;j++)
            BP[j]=0;
        
        for(int j=1; j<= (windowLength/2+1);j++)
        {
            double freq=j*freqStep;
            if(freq >250 && freq < 6000)
            {
                int pindex=floor((freq-250)/575);
                double cp= sqrt(fftcurrent[j-1].real*fftcurrent[j-1].real+ fftcurrent[j-1].image*fftcurrent[j-1].image);
                BP[pindex]+=cp;
                sumP+=cp;
            }
        }
        
        for(int j=0; j< EntropypartCount; j++)
        {
            if(sumP==0)
            {
                BP[j]=0;
            }
            else
            {
                BP[j]=BP[j]/sumP;
            }
        }
        
        for (int j=0; j<EntropypartCount; j++)
        {
            if(BP[j]<=0) continue;
            result[i]-=BP[j]*log(BP[j]);
        }
        delete[] BP;
    }
    
    for(int a=1;a<windowCount;a++){
        result[a]=0.94*result[a-1]+(1-0.94)*result[a];
    }
    
    result=UnitaryList(result);
    
    return result;
}

List<HillUnit> SplitEndpoint(List<double> Entropylist, List<double> PowerPerFrame, int FS)
{
    List<double> newline;
    List<double> guiyi=UnitaryList(PowerPerFrame);
    for(int i=0;i< Entropylist.Count(); i++)
    {
        newline.Add((1-Entropylist[i])*guiyi[i]);
    }
    List<HillUnit> result=FindHills(newline);
    
    for(int i=result.Count()-1; i>=0; i--)
    {
        double power=0;
        for( int j=result[i].start; j<=result[i].end; j++)
        {
            power+=newline[j];
        }
        
        if(power<0.0005)
        {
            result.Remove(i);
            continue;
        }
        
        //printf("%d~%d %f\n",result[i].start,result[i].end, power);
    }
    
    return result;
}

double linetomelFreq(double lineFreq)
{
    double k=1127.01048;
    double af=fabs(lineFreq);
    return sign(lineFreq)*log(1+af/700)*k;
}

double meltolineFreq(double melFreq)
{
    return 700*(exp(melFreq/1127.01048)-1);
}

List<double> FFTPowerList(VectorList<COMPLEX> FFTResult, int start, int end)
{
    int col=FFTResult.Width;
    
    List<double> powerresult(col/2+1,0);
    
    for(int i=start ; i< end; i++)
    {
        List<COMPLEX> temp=FFTResult.Data[i];
        
        
        for( int j=0; j< col/2+1; j++)
        {
            
            if(j==0){
                powerresult[j]+=sqrt(temp[j].real*temp[j].real+temp[j].image*temp[j].image);
            }else
            {
                powerresult[j]+=sqrt(temp[j].real*temp[j].real*4+temp[j].image*temp[j].image*4);
            }
            
        }
        
    }
    
    for( int i=0; i<col/2+1;i++)
    {
        powerresult[i]=20*log(powerresult[i]);
    }
    return powerresult;
}

bool compare(double a, double b)
{
    return fabs(a) < fabs(b);
}


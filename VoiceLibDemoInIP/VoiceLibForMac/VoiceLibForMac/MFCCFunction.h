//
//  MFCCFunction.h
//  MacTest
//
//  Created by 姜珂 on 14-10-16.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#ifndef MacTest_MFCCFunction_h
#define MacTest_MFCCFunction_h
#include "List.h"
#include "Vector.h"
#include <stdlib.h>
#include "Structions.h"

//int INTMIN(int a, int b);

double GetMFCCPoint(double** mfcc1,int mfcc1start,int mfcc1end,double** mfcc2, int mfcc2start, int mfcc2end);

VectorList<double> melbank(int banklevel,int frameLen,int FS, double fl, double fh);

void GetMFCCFilter(VectorList<double> &bank, VectorList<double> &dctcoef, List<double> &w);

bool MatchAudioWithChars(char *aChars, int aCharsLength, List<HillUnit> bAudiolist, List<MatchHillUnit> & result);

int wordMatch(int *aAudio, int *aWord, int aLen, int *bAudio, int *bWord, int bLen, int result[]);
//

#endif

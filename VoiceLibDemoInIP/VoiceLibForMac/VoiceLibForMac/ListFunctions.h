//
//  ListFunctions.h
//  MacTest
//
//  Created by 姜珂 on 14-10-16.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#ifndef MacTest_ListFunctions_h
#define MacTest_ListFunctions_h
#include "List.h"
#include "Vector.h"
#include "Structions.h"


///unitary
List<double> UnitaryList(List<double> inputlist);

VectorList<double> UnitaryVectorList(VectorList<double> input);

double Max(VectorList<double> inputlist);

double Sum(List<double> input);

template <class T>
T* GetArrayList(List<T> input)
{
    T* r=new T[input.Count()];
    for( int i=0; i<input.Count(); i++)
    {
        r[i]=input[i];
    }
    return r;
}

template <class T>
double* GetArrayList(List<T> input, int start, int count)
{
    T* r=new T[count];
    for( int i=start; i<start+count; i++)
    {
        r[i-start]=input[i];
    }
    return r;
}

VectorList<double> VectorTimes(VectorList<double> a, VectorList<double> b);

double Sum(VectorList<double> input);

double SumColumn(VectorList<double> input,int Cindex);

double Dot( List<double> a, List<double> b);

int sign(double x);

List<HillUnit> FindHills(List<double> input);
#endif

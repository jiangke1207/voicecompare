//
//  MFCCFunction.cpp
//  VoiceLibForMac
//
//  Created by 姜珂 on 14/10/28.
//  Copyright (c) 2014年 Xiaoma. All rights reserved.
//


#include <math.h>
#include "MFCCFunction.h"
#include "OFunctions.h"


int INTMIN(int a, int b)
{
    if(a>b)
    {
        return b;
    }else
    {
        return a;
    }
}

double GetMFCCPoint(List<double*> mfcc1, List<double*> mfcc2)
{
    
    //int index=MIN(mfcc1.Count(), mfcc2.Count());
    double sum=0;
    //    for(int i=0 ;i<index; i++)
    //    {
    //        for(int j=12;j<24;j++)
    //        {
    //            sum+=(mfcc1[i][j]- mfcc2[i][j])*(mfcc1[i][j]- mfcc2[i][j]);
    //        }
    //    }
    //
    //    sum=sqrt(sum);
    
    return sum;
}

VectorList<double> melbank(int banklevel,int frameLen,int FS, double fl, double fh)
{
    List<double> mflh;
    mflh.Add(fl*FS);
    mflh.Add(fh*FS);
    
    for(int i=0; i<mflh.Count();i++)
    {
        mflh[i]=linetomelFreq(mflh[i]);
    }
    
    double melrng=mflh[1];
    
    int fn2=floor(frameLen/2);
    
    double melinc=melrng/(banklevel+1);
    
    List<double> blim;
    blim.Add(0);
    blim.Add(1);
    blim.Add(banklevel);
    blim.Add(banklevel+1);
    
    for(int i=0; i<blim.Count();i++)
    {
        blim[i]=meltolineFreq(blim[i]*melinc+mflh[0])*frameLen/FS;
    }
    
    List<double> mc;
    for(int i=0; i<banklevel; i++)
    {
        mc.Add(mflh[0]+(i+1)*melinc);
    }
    
    int b1=floor(blim[0])+1;
    int b4=INTMIN(fn2, ceil(blim[3])-1);
    
    List<double> pf;
    List<int> fp;
    List<double> pm;
    for(int i=b1; i<=b4; i++)
    {
        pf.Add( (linetomelFreq(i*FS/(double)frameLen)-mflh[0])/melinc);
        fp.Add(floor(pf[i-b1]));
        pm.Add(pf[i-b1]-fp[i-b1]);
    }
    
    int k2=0;
    for(int i=0;i< fp.Count();i++)
    {
        if(fp[i]>0)
        {
            k2=i;
            break;
        }
    }
    
    int k3=0;
    for(int i=fp.Count()-1; i>=0; i--)
    {
        if(fp[i]<banklevel)
        {
            k3=i;
            break;
        }
    }
    
    int k4=fp.Count();
    
    List<int> r;
    for(int i=0;i<=k3;i++)
    {
        r.Add(fp[i]+1);
    }
    
    for(int i=k2; i<=k4-1; i++)
    {
        r.Add(fp[i]);
    }
    
    List<int> c;
    for(int i=1;i<=k3;i++)
    {
        c.Add(i);
    }
    
    for(int i=k2; i<=k4; i++)
    {
        c.Add(i);
    }
    
    List<double> v;
    for(int i=0;i<=k3;i++)
    {
        v.Add(pm[i]);
    }
    
    for(int i=k2; i<=k4-1; i++)
    {
        v.Add(1-pm[i]);
    }
    
    int mn=b1+1;
    int mx=b4+1;
    
    for (int i=0; i<v.Count(); i++) {
        v[i]=0.5-0.46/1.08*cos(v[i]*pi);
    }
    
    
    for(int i=0;i<c.Count();i++)
    {
        if((c[i]+mn >2) &&(c[i]+mn<frameLen-fn2+2))
        {
            v[i]=v[i]*2;
        }
    }
    
    VectorList<double> result(banklevel,1+fn2,0);
    
    for(int i=0; i< r.Count();i++)
    {
        result.Data[r[i]-1][c[i]+mn-2]=v[i];
        //printf("%d , %d = %f \n",r[i],c[i]+mn-1,v[i]);
    }
    
    return result;
}

void GetMFCCFilter(VectorList<double> &bank, VectorList<double> &dctcoef, List<double> &w)
{
    double max=Max(bank);
    
    for(int i=0;i< bank.Heigth;i++)
    {
        for(int j=0;j<bank.Width;j++)
        {
            bank.Data[i][j]=bank.Data[i][j]/max;
        }
    }
    
    
    for(int k=0;k<12;k++)
    {
        for(int n=0;n<24; n++)
        {
            dctcoef.Data[k][n]=cos((2*n+1)*(k+1)*pi/(2*24));
        }
    }
    
    
    for(int i=1; i<=12; i++)
    {
        w.Add(1+6*sin(pi*i/12));
    }
    
    w=UnitaryList(w);
}

bool MatchAudioWithChars(char *aChars, int aCharsLength, List<HillUnit> bAudiolist, List<MatchHillUnit> & result)
{
    List<HillUnit> aAudiolist;
    
    int aCharsStar = 0;
    
    for (int i=0; i<aCharsLength; i++) {
        if (aChars[i] == ' ') {
            HillUnit tempAHill;
            tempAHill.start = aCharsStar;
            tempAHill.end = i;
            aAudiolist.Add(tempAHill);
            
            aCharsStar = i +1;
        }
    }
    
    if (aCharsStar < aCharsLength -1) {
        HillUnit tempAHill;
        tempAHill.start = aCharsStar;
        tempAHill.end = aCharsLength -1;
        aAudiolist.Add(tempAHill);
    }
    
    if (aAudiolist.Count() <= 0 || bAudiolist.Count() <= 0) {
        return false;
    }
    
    double aAudioLength = aAudiolist[aAudiolist.Count() -1].end - aAudiolist[0].start;
    double bAudioLength = bAudiolist[bAudiolist.Count() -1].end - bAudiolist[0].start;
    
    List<Percentage> perA;
    List<Percentage> perB;
    
    double perAStar = aAudiolist[0].start;
    double perBStar = bAudiolist[0].start;
    
    for (int i=0; i<aAudiolist.Count(); i++) {
        Percentage tempPer;
        tempPer.start = (((double)aAudiolist[i].start) - perAStar)/ aAudioLength;
        tempPer.end = (((double)aAudiolist[i].end) - perAStar)/ aAudioLength;
        perA.Add(tempPer);
    }
    
    for (int i=0; i<bAudiolist.Count(); i++) {
        Percentage tempPer;
        tempPer.start = (((double)bAudiolist[i].start) - perBStar) / bAudioLength;
        tempPer.end = (((double)bAudiolist[i].end) - perBStar) / bAudioLength;
        perB.Add(tempPer);
    }
    
    List<double> matchResult;
    
    for (int i=0; i<aAudiolist.Count(); i++) {
        //        matchResult[mr] = perA[i].start;
        //        mr++;
        double end = perA[i].end;
        matchResult.Add(end);
    }
    
    for (int i=0; i<bAudiolist.Count(); i++) {
        //        matchResult[mr] = perB[i].start * -1;
        //        mr++;
        
        double end = perB[i].end * -1;
        matchResult.Add(end);
    }
    
    std::sort(matchResult.begin(), matchResult.end(), compare);
    
    List<MatchAB> mAB;
    
    int aa = 0;
    int bb = 0;
    
    for (int i=0; i<matchResult.Count(); i++) {
        if (matchResult[i] > 0) {
            aa++;
        }
        else {
            bb ++;
        }
        
        if (bb > 0) {
            //现在指向一个B的end点
            double eValue = 0.0;
            
            if (aa > 0) {
                //前面有A
                eValue = fabs(matchResult[i]) - fabs(matchResult[i-1]);
                
                if (i < matchResult.Count() -1) {
                    //没到数组末尾 / /
                    if (matchResult[i+1] > 0) {
                        //下一个是A
                        if (eValue > fabs(matchResult[i + 1]) - fabs(matchResult[i])) {
                            i ++;
                            aa++;
                        }
                    }
                    
                    if (mAB.Count() > 0) {
                        MatchAB tempMAB;
                        
                        tempMAB.A1 = mAB.back().A2 +1;
                        tempMAB.A2 = mAB.back().A2 +aa;
                        tempMAB.B1 = mAB.back().B2 +1;
                        tempMAB.B2 = mAB.back().B2 +bb;
                        
                        mAB.Add(tempMAB);
                    }
                    else {
                        
                        MatchAB tempMAB;
                        
                        tempMAB.A1 = 0;
                        tempMAB.A2 = aa - 1;
                        tempMAB.B1 = 0;
                        tempMAB.B2 = bb -1;
                        
                        mAB.Add(tempMAB);
                    }
                    
                    aa = 0;
                    bb = 0;
                }
                else {
                    //到末尾了
                    if (mAB.Count() > 0) {
                        MatchAB tempMAB;
                        
                        tempMAB.A1 = mAB.back().A2 +1;
                        tempMAB.A2 = mAB.back().A2 +aa;
                        tempMAB.B1 = mAB.back().B2 +1;
                        tempMAB.B2 = mAB.back().B2 +bb;
                        
                        mAB.Add(tempMAB);
                    }
                    else {
                        MatchAB tempMAB;
                        
                        tempMAB.A1 = 0;
                        tempMAB.A2 = aa - 1;
                        tempMAB.B1 = 0;
                        tempMAB.B2 = bb -1;
                        
                        mAB.Add(tempMAB);
                    }
                    
                    aa = 0;
                    bb = 0;
                    break;
                }
            }
            else {
                //前面没有A
                if (i < matchResult.Count() -1) {
                    //没到数组末尾
                    if (matchResult[i+1] > 0) {
                        //下一个是A
                        i++;
                        aa++;
                        eValue = fabs(matchResult[i]) - fabs(matchResult[i-1]);
                        
                        if (i < matchResult.Count() -1) {
                            //没到末尾
                            if (matchResult[i+1] < 0) {
                                //下一个是B
                                if (eValue > fabs(matchResult[i +1]) - fabs(matchResult[i])) {
                                    i ++;
                                    bb++;
                                }
                            }
                            
                            if (mAB.Count() > 0) {
                                MatchAB tempMAB;
                                
                                tempMAB.A1 = mAB.back().A2 +1;
                                tempMAB.A2 = mAB.back().A2 +aa;
                                tempMAB.B1 = mAB.back().B2 +1;
                                tempMAB.B2 = mAB.back().B2 +bb;
                                
                                mAB.Add(tempMAB);
                            }
                            else {
                                MatchAB tempMAB;
                                
                                tempMAB.A1 = 0;
                                tempMAB.A2 = aa - 1;
                                tempMAB.B1 = 0;
                                tempMAB.B2 = bb -1;
                                
                                mAB.Add(tempMAB);
                            }
                            
                            aa = 0;
                            bb = 0;
                        }
                        else {
                            //到末尾了
                            if (mAB.Count() > 0) {
                                MatchAB tempMAB;
                                
                                tempMAB.A1 = mAB.back().A2 +1;
                                tempMAB.A2 = mAB.back().A2 +aa;
                                tempMAB.B1 = mAB.back().B2 +1;
                                tempMAB.B2 = mAB.back().B2 +bb;
                                
                                mAB.Add(tempMAB);
                            }
                            else {
                                MatchAB tempMAB;
                                
                                tempMAB.A1 = 0;
                                tempMAB.A2 = aa - 1;
                                tempMAB.B1 = 0;
                                tempMAB.B2 = bb -1;
                                
                                mAB.Add(tempMAB);
                            }
                            
                            aa = 0;
                            bb = 0;
                            break;
                        }
                    }
                }
                else {
                    //到数组末尾了
                    if (mAB.Count() > 0) {
                        mAB.back().B2 += bb;
                        break;
                    }
                    else {
                        return false;
                    }
                }
            }
        }
        else {
            if (i == matchResult.Count() -1) {
                //到数组末尾了
                if (mAB.Count() > 0) {
                    mAB.back().A2 += aa;
                    break;
                }
                else {
                    return false;
                }
            }
        }
    }
    
    
    for (int i=0; i<matchResult.Count(); i++) {
        
    }
    
    double lengthDeviationValue = 0.0;
    double perDeviationValue = 0.0;
    
    printf("共%d组:\n", matchResult.Count());
    
    for (int i=0; i<mAB.Count(); i++) {
        
        MatchHillUnit tempMHU;
        tempMHU.hillUnitAStar = aAudiolist[mAB[i].A1].start;
        tempMHU.hillUnitAEnd = aAudiolist[mAB[i].A2].end;
        
        tempMHU.hillUnitBStar = bAudiolist[mAB[i].B1].start;
        tempMHU.hillUnitBEnd = bAudiolist[mAB[i].B2].end;
        
        double aLength = aAudiolist[mAB[i].A2].end - aAudiolist[mAB[i].A1].start;
        double bLength = bAudiolist[mAB[i].B2].end - bAudiolist[mAB[i].B1].start;
        
        tempMHU.lengthDeviationValue = aLength > bLength ? ((aLength - bLength) / bLength) : ((bLength - aLength) / aLength);
        tempMHU.perDeviationValue =  fabs(fabs(perA[mAB[i].A1].start) - fabs(perB[mAB[i].B1].start));
        
        result.Add(tempMHU);
        
        lengthDeviationValue += tempMHU.lengthDeviationValue;
        perDeviationValue += tempMHU.perDeviationValue;
        
        printf("A(%d,%d) B(%d,%d) ~[%d,%d]~ [%d,%d]\n", mAB[i].A1, mAB[i].A2, mAB[i].B1, mAB[i].B2
               , aAudiolist[mAB[i].A1].start,aAudiolist[mAB[i].A2].end,bAudiolist[mAB[i].B1].start,
               bAudiolist[mAB[i].B2].end);
    }
    
    printf("总长度偏差值:%lf\n", lengthDeviationValue);
    printf("总百分比偏差值:%lf\n", perDeviationValue);
    
    return true;
}

int wordMatch(int *aAudio, int *aWord, int aLen, int *bAudio, int *bWord, int bLen, int result[])
{
    int start = 0;
    
    int ri = 0;
    int ai = 1;
    int bi = 1;
    while (1) {
        if (aWord[ai] < bWord[bi]) {
            ai += 2;
        }
        else if (aWord[ai] > bWord[bi]) {
            bi += 2;
        }
        else {
            result[ri] = aAudio[start];
            ri ++;
            
            result[ri] = aAudio[ai];
            ri ++;
            
            result[ri] = bAudio[start];
            ri ++;
            
            result[ri] = bAudio[bi];
            ri ++;
            
            start = ai+1;
            
            ai += 2;
            bi += 2;
        }
        
        if (ai > aLen *2 || bi > bLen *2) {
            break;
        }
    }
    
    return ri;
}

double OshiDestance(double* mfcc1, double* mfcc2, int weishu)
{
    double sum=0;

    for(int i=0;i<weishu;i++)
    {
        sum+=((mfcc1[i]- mfcc2[i])*(mfcc1[i]- mfcc2[i]));
    }

    return sqrt(sum);
}

double GetMFCCPoint(double** mfcc1,int mfcc1start,int mfcc1end,double** mfcc2, int mfcc2start, int mfcc2end)
{
    VectorList<double> ResultVector(mfcc1end-mfcc1start+1, mfcc2end-mfcc2start+1,0);
    
    for(int i=mfcc1start; i<=mfcc1end;i++)
    {
        for (int j=mfcc2start; j<=mfcc2end;j++)
        {
            ResultVector.Data[i-mfcc1start][j-mfcc2start]=OshiDestance(mfcc1[i], mfcc2[j], 24);
            //printf("%f \n",input.PowerOfWindows[i]);
        }
    }
    
    //ResultVector=UnitaryVectorList(ResultVector);
    
    int ViewWidth=3;
    
    VectorList<FrameMatch> Matches(mfcc1end-mfcc1start+1, mfcc2end-mfcc2start+1, FrameMatch());
    for(int i=0; i<5;i++)
    {
        Matches.Data[i][0].Distance=OshiDestance(mfcc1[i], mfcc2[0], 24);
        Matches.Data[i][0].Hasvalue=true;
    }
    
    for(int i=0; i<5;i++)
    {
        Matches.Data[0][i].Distance=OshiDestance(mfcc1[0], mfcc2[i], 24);
        Matches.Data[0][i].Hasvalue=true;
    }
    
    for(int i=1; i<mfcc1end-mfcc1start+1;i++)
    {
        for (int j=1; j<mfcc2end-mfcc2start+1;j++)
        {
            if(Matches.Data[i][j].Hasvalue)
                continue;
            
            int xx=0;
            int yy=0;
            int zz=99999;
            
            for(int x=i-2;x<i;x++)
            {
                for(int y=j-2;y<j;y++)
                {
                    if(x>=0 && y>=0 && Matches.Data[x][y].Hasvalue)
                    {
                        if(zz> Matches.Data[x][y].Distance)
                        {
                            xx=x;
                            yy=y;
                            zz=Matches.Data[x][y].Distance;
                        }
                    }
                }
            }
            
            Matches.Data[i][j].lastA=xx;
            Matches.Data[i][j].lastB=yy; 
            Matches.Data[i][j].Distance=zz+OshiDestance(mfcc1[xx], mfcc2[yy], 24);
            Matches.Data[i][j].Count=Matches.Data[xx][yy].Count+1;
            Matches.Data[i][j].Hasvalue=true;
        }
    }
    
    int mfcc1count=mfcc1end-mfcc1start+1;
    int mfcc2count=mfcc2end-mfcc2start+1;
    
    FrameMatch d=Matches.Data[mfcc1count-1][mfcc2count-1];
    for( int i=mfcc1count-1; i>=mfcc1count-6;i--)
    {
        if(d.Distance> Matches.Data[i][mfcc2count-1].Distance)
            d=Matches.Data[i][mfcc2count-1];
    }
    for( int i=mfcc2count-1; i>=mfcc2count-6;i--)
    {
        if(d.Distance< Matches.Data[mfcc1count-1][i].Distance)
            d=Matches.Data[mfcc1count-1][i];
    }
    
    return  d.Distance;
}



//
//  Structions.h
//  MacTest
//
//  Created by 姜珂 on 14/10/27.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#ifndef MacTest_Structions_h
#define MacTest_Structions_h


typedef struct
{
    double real ;
    double image ;
} COMPLEX ;

typedef unsigned char Byte;

typedef struct
{
    int start;
    int end;
    int center;
    double centerValue;
    int framecount;
    
    double Powersum;
    double BilvofTotleForTime;
    int tempforXielv;
    
}HillUnit;

typedef struct
{
    double start;
    double end;
    
}Percentage;

typedef struct
{
    int A1;
    int A2;
    int B1;
    int B2;
    
}MatchAB;

typedef struct
{
    int hillUnitAStar;
    int hillUnitAEnd;
    
    int hillUnitBStar;
    int hillUnitBEnd;
    
    double lengthDeviationValue;
    double perDeviationValue;
}MatchHillUnit;

typedef struct
{
    int frameA;
    int frameB;
    double Distance;
    
    int lastA=-1;
    int lastB=-1;
    int Count=1;
    bool Hasvalue=false;
    
} FrameMatch;

typedef struct
{
    double version;
    
    int wordLen;
    char* Words;
    
    int FrameCount;
    
    int WordsCutCount;
    int* WordsCut;
    int* FrameCut;
    
    double** MFCC;
    double* PowerfulList;
    
    int wavelen;
    
    Byte* wavfile;
}FormatFile;

typedef struct
{
    bool IsMatched;
    
    int ErrorCode=0;
    
    int* WordsMatchedPoint;
    int* WordSplitCount;
    
    int WordCount;
    
    int PitchPoint;
    int PowerPoint;
    
    int point;
}CRuslt;

#endif

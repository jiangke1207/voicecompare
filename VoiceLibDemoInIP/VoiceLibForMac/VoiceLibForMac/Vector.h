//
//  Vector.h
//  MacTest
//
//  Created by 姜珂 on 14-10-15.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#ifndef MacTest_Vector_h
#define MacTest_Vector_h

#include "List.h"

template<class T>
class VectorList
{
public:
    List<List<T>> Data;
    
    int Heigth=0;
    int Width=0;
    
    VectorList(int height, int width, T defaultvalue)
    {
        for(int i=0; i<height; i++)
        {
            List<T> a;
            for(int j=0; j<width; j++)
            {
                a.Add(defaultvalue);
            }
            Data.Add(a);
        }
        
        Heigth=height;
        Width=width;
    }
    
    List<T> GetColumn(int t)
    {
        List<T> result;
        for(int i=0;i< Heigth;i++)
        {
            result.Add( Data[i][t]);
        }
        
        return result;
    }
    
    
    
};

#endif

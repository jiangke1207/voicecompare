//
//  OFunctions.h
//  MacTest
//
//  Created by 姜珂 on 14-10-15.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#ifndef MacTest_OFunctions_h
#define MacTest_OFunctions_h
#include "List.h"
#include "Vector.h"
#include "Structions.h"
#include "ListFunctions.h"
#include <math.h>

const double pi=3.1415926;
const int EntropypartCount=20;

///预加重
List<double> PreEmphasis(List<double> input);

///汉明窗
List<double> GetHamming(int len);

///add window
VectorList<double> AddWindow(List<double> input, int windowsLen);

///FFT
List<COMPLEX> FFT(List<double> input,int inputlength);

///FFT for every frame, freqlist is output for Frequency of every number
VectorList<COMPLEX> FFTWindowfulArray(VectorList<double> windowfulinput,int fs, List<double> &freqlist);

///Power sum in every frame
List<double> PowerPerFrame(VectorList<double> windowsfulinput);

///Get Entropy
List<double> GetEntropy(VectorList<COMPLEX> fftfulinput,int FS);

List<HillUnit> SplitEndpoint(List<double> Entropylist, List<double> PowerPerFrame, int FS);

double linetomelFreq(double lineFreq);

double meltolineFreq(double melFreq);

List<double> FFTPowerList(VectorList<COMPLEX> FFTResult, int start, int end);

bool compare(double a, double b);


#endif

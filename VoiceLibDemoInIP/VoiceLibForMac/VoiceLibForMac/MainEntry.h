//
//  MainEntry.h
//  MacTest
//
//  Created by 姜珂 on 14/10/25.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#ifndef MacTest_MainEntry_h
#define MacTest_MainEntry_h
#include "Structions.h"

FormatFile GetWavFile(Byte* FormatFileBytes, int len);

FormatFile ParseFormatFile(Byte* FileBytes, int len, char words[],int wordlen);

CRuslt GetPointResult(Byte* F_File,int FormatFileLen, Byte* waveFileofUser, int WaveFileLen);

Byte* CreatedFormatFile(Byte* waveFileByte, char words[],int wordlen, int wavefileLength, int &OutLength);


#endif

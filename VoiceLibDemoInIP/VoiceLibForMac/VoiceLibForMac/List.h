//
//  List.h
//  MacTest
//
//  Created by 姜珂 on 14-10-15.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#ifndef __MacTest__List__
#define __MacTest__List__
#include <vector>
#include <stdio.h>
template<class T>
class List: public std::vector<T>
{
public:
    int Count()
    {
        return this->size();
    }
    
    void Add(T t)
    {
        this->push_back(t);
    }
    
    void AddList(List<T> v)
    {
        for(int i=0;i<v.Count();i++)
        {
            Add(v[i]);
        }
    }
    
    void Remove(int index)
    {
        this->erase(this->begin()+index);
    }
    
    List()
    {
    
    }
    
    List(int size,T defaultvalue)
    {
        for(int i=0; i< size; i++)
            Add(defaultvalue);
    }
    
    List(T* arraylist, int count)
    {
        for(int i=0; i<count; i++)
            Add(arraylist[i]);
    }
    
    ~List()
    {

    }
    
    
};


#endif /* defined(__MacTest__List__) */

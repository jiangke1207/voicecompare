//
//  FileCodeOperator.cpp
//  VoiceLibForMac
//
//  Created by 姜珂 on 14/10/28.
//  Copyright (c) 2014年 Xiaoma. All rights reserved.
//

#include <stdio.h>
#include "FileCodeOperator.h"

char* Getstring(Byte* datas, int start, int count, int &index)
{
    char* string=new char[count];
    for(int i=0;i<count;i++)
    {
        string[i]=datas[start+i];
    }
    index+=count;
    return string;
}

int GetInt(Byte* datas, int start, int count, int &index)
{
    int result=0;
    for(int i=0;i<count;i++)
    {
        result+=(datas[start+i] & 0xFF) << (8*i);
    }
    index+=count;
    return result;
}

double Getdouble(Byte* datas, int start, int count, int &index)
{
    double b;
    
    Byte *pf=(Byte*)&b;
    
    for (int i=0; i<count; i++) {
        pf[i]=datas[i+start];
    }
    
    index+=count;
    return b;
}

template <class T>
List<Byte> ConvertToByte(T v)
{
    List<Byte> Bytelist;
    Byte* pdata = (Byte*)&v;
    
    for(int i=0;i< sizeof(T);i++)
    {
        Bytelist.Add(pdata[i]);
    }
    
    return Bytelist;
}

int strlen(char word[])
{
    int len=0;
    while (*word++) {
        len++;
    }
    
    return len;
}

//解析wav文件
List<double> DecodeWaveFile(Byte* filedatas, int len, int &BitPerSimple, int &ChannelCount, int &SimplesPerSecond)
{
    int index=0;
    Getstring(filedatas, index, 4 , index);
    GetInt(filedatas, index, 4, index);
    Getstring(filedatas, index, 4, index);
    
    Getstring(filedatas, index, 4, index);
    int formatchunksize=GetInt(filedatas, index, 4, index);
    GetInt(filedatas, index, 2, index);
    ChannelCount=GetInt(filedatas, index, 2, index);
    SimplesPerSecond=GetInt(filedatas, index, 4, index);;
    GetInt(filedatas, index, 4, index);
    GetInt(filedatas, index, 2, index);
    BitPerSimple=GetInt(filedatas, index, 2, index);
    
    
    if(formatchunksize==18)
        GetInt(filedatas, index, 2, index);
    
    while(true)
    {
        char* temp=Getstring(filedatas, index, 4, index);
        if(temp[0]=='d'&temp[1]=='a'&temp[2]=='t'&temp[3]=='a')
        {
            Getstring(filedatas, index, 4, index);
            break;
        }
        
    }
    
    int persimpleByteLen=ChannelCount *(BitPerSimple/8);
    int SimpleCount=(len-index)/persimpleByteLen;
    
    List<double> orgData;
    
    for( int i=index; i<len; i=i+persimpleByteLen)
    {
        int o=0;
        o=(filedatas[i+1]<<8)+filedatas[i];
        
        if(o>=32768) o=-(65536-o);
        
        orgData.Add(o/32768.0);
    }
    
    return orgData;
}

//保存FormatFile
List<Byte> CreateFormatFile(FormatFile formatfile)
{
    List<Byte> output;
    
    output.AddList(ConvertToByte('F'));
    output.AddList(ConvertToByte(formatfile.version));
    int wordcount=strlen(formatfile. Words);
    output.AddList(ConvertToByte(wordcount));
    for (int i=0; i< wordcount; i++)
    {
        output.AddList(ConvertToByte(formatfile.Words[i]));
    }
    
    output.AddList(ConvertToByte(formatfile.FrameCount));
    
    output.AddList(ConvertToByte(formatfile.WordsCutCount));
    int wordcutcount= formatfile.WordsCutCount;
    for(int i=0; i<wordcutcount;i++)
    {
        output.AddList(ConvertToByte(formatfile.WordsCut[2*i]));
        output.AddList(ConvertToByte(formatfile.WordsCut[2*i+1]));
        output.AddList(ConvertToByte(formatfile.FrameCut[2*i]));
        output.AddList(ConvertToByte(formatfile.FrameCut[2*i+1]));
    }
    
    for (int i=0; i<formatfile.FrameCount; i++) {
        for(int j=0;j<24;j++)
        {
            output.AddList(ConvertToByte(formatfile.MFCC[i][j]));
        }
        output.AddList(ConvertToByte(formatfile.PowerfulList[i]));
    }
    
    output.AddList(ConvertToByte(formatfile.wavelen));
    
    int iii=formatfile.wavelen;
    
    for(int i=0; i< formatfile.wavelen; i++)
        output.Add(formatfile.wavfile[i]);
    
    //output.AddList(ConvertToByte(Words));
    
    int d=output.Count();
    //Words[0]='P';
    
    return output;
}

//读取FormatFile
FormatFile DecodeFormatFile(Byte* FileInput, int len)
{
    int index=0;
    char* first= Getstring(FileInput, index, 1 , index);
    if(first[0]=='F')
    {
        FormatFile FF;
        
        FF.version=Getdouble(FileInput, index, 8, index);
        FF.wordLen=GetInt(FileInput, index, 4, index);
        FF.Words=new char[FF.wordLen];
        for(int i=0; i<FF.wordLen; i++)
        {
            FF.Words[i]=(char)FileInput[i+index];
        }
        index+=FF.wordLen;
        
        FF.FrameCount=GetInt(FileInput, index, 4, index);
        
        FF.WordsCutCount=GetInt(FileInput, index, 4, index);
        
        FF.WordsCut=new int[FF.WordsCutCount*2];
        FF.FrameCut=new int[FF.WordsCutCount*2];
        for(int i=0; i< FF.WordsCutCount; i++)
        {
            FF.WordsCut[2*i]=GetInt(FileInput, index, 4, index);
            FF.WordsCut[2*i+1]=GetInt(FileInput, index, 4, index);
            FF.FrameCut[i*2]=GetInt(FileInput, index, 4, index);
            FF.FrameCut[i*2+1]=GetInt(FileInput, index, 4, index);
        }
        
        FF.MFCC=new double*[FF.FrameCount];
        FF.PowerfulList=new double[FF.FrameCount];
        for (int i=0; i<FF.FrameCount; i++) {
            FF.MFCC[i]=new double[24];
            for(int j=0;j<24;j++)
            {
                FF.MFCC[i][j]=Getdouble(FileInput, index, 8, index);
            }
            FF.PowerfulList[i]=Getdouble(FileInput, index, 8, index);
        }
        
        FF.wavelen=GetInt(FileInput, index, 4, index);
        
        
        FF.wavfile=new Byte[len-index];
        for(int i=index; i<len; i++)
        {
            FF.wavfile[i-index]=FileInput[i];
        }
        
        return FF;
    }
    
    return FormatFile();
}
//
//  FileCodeOperator.h
//  MacTest
//
//  Created by 姜珂 on 14/10/26.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#ifndef MacTest_FileCodeOperator_h
#define MacTest_FileCodeOperator_h

#include "List.h"
#include "Vector.h"
#include "Structions.h"

char* Getstring(Byte* datas, int start, int count, int &index);

int GetInt(Byte* datas, int start, int count, int &index);

double Getdouble(Byte* datas, int start, int count, int &index);

List<double> DecodeWaveFile(Byte* filedatas, int len, int &BitPerSimple, int &ChannelCount, int &SimplesPerSecond);

int strlen(char word[]);

List<Byte> CreateFormatFile(FormatFile formatfile);

FormatFile DecodeFormatFile(Byte* FileInput, int len);
#endif

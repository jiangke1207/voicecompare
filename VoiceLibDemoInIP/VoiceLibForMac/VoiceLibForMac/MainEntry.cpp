//
//  MainEntry.cpp
//  VoiceLibForMac
//
//  Created by 姜珂 on 14/10/28.
//  Copyright (c) 2014年 Xiaoma. All rights reserved.
//

#include <stdio.h>
#include "MainEntry.h"
#include "OFunctions.h"
#include "Structions.h"
#include "MFCCFunction.h"
#include "FileCodeOperator.h"
#include "ListFunctions.h"

FormatFile GetWavFile(Byte* FormatFileBytes, int len)
{
    return DecodeFormatFile(FormatFileBytes, len);
}

FormatFile ParseFormatFile(Byte* FileBytes, int len, char words[],int wordlen)
{
    int ChannelCount;
    int SimplesPerSecond;
    int BitPerSimple;
    
    List<double> filedata=DecodeWaveFile(FileBytes, len, BitPerSimple, ChannelCount, SimplesPerSecond);
    
    VectorList<double> bank= melbank(24, 256, 8000, 0, 0.4);
    
    VectorList<double> dctcoef(12,24,0);
    List<double> w;
    GetMFCCFilter(bank,dctcoef,w);
    
    List<double> EmphasisData = PreEmphasis(filedata);
    
    VectorList<double> AddwindowData= AddWindow(EmphasisData, 256);
    
    List<double> freqlist;
    
    VectorList<COMPLEX> FFTresult=FFTWindowfulArray(AddwindowData, SimplesPerSecond, freqlist);
    
    VectorList<double> MFCCResult(FFTresult.Heigth,12,0);
    
    for(int i=0; i< FFTresult.Heigth; i++)
    {
        VectorList<double> t(129,1,0);
        for(int j=0; j<129; j++)
        {
            t.Data[j][0]=FFTresult.Data[i][j].image*FFTresult.Data[i][j].image
            +FFTresult.Data[i][j].real*FFTresult.Data[i][j].real;
        }
        
        VectorList<double> ctemp=VectorTimes(bank,t);
        for(int qq=0; qq< ctemp.Heigth;qq++)
        {
            for(int ww=0; ww< ctemp.Width;ww++)
            {
                ctemp.Data[qq][ww]=log(ctemp.Data[qq][ww]);
                //printf(" %f\n",ctemp.Data[qq][ww]);
            }
        }
        ctemp=VectorTimes(dctcoef, ctemp);
        
        for(int j=0; j<12; j++)
        {
            MFCCResult.Data[i][j]=ctemp.Data[j][0]* w[j];
            //printf("%d,%d  : %f %f\n",i,j ,0.0,ctemp.Data[j][0]);
        }
        
    }
    
    VectorList<double> dtm(FFTresult.Heigth,24,0);
    for(int i=2; i<FFTresult.Heigth-2;i++)
    {
        
        for(int j=0; j< 12; j++)
        {
            dtm.Data[i][12+j]=-2*MFCCResult.Data[i-2][j]-MFCCResult.Data[i-1][j]+ MFCCResult.Data[i+1][j]+2*MFCCResult.Data[i+2][j];
            dtm.Data[i][12+j]/=3;
            //printf("%d,%d  :  %f\n",i,j ,dtm.Data[i][j]);
        }
    }
    
    for(int i=0; i<FFTresult.Heigth; i++)
    {
        for(int j=0; j< 12; j++)
        {
            dtm.Data[i][j]=MFCCResult.Data[i][j];
        }
    }
    
    List<double> EntropyResultList=GetEntropy(FFTresult, SimplesPerSecond);
    
    List<double> PowerofFrame=PowerPerFrame(AddwindowData);
    
    List<HillUnit> GetHills=SplitEndpoint(EntropyResultList, PowerofFrame, SimplesPerSecond);
    
    List<MatchHillUnit> matchresult;
    bool matchr=MatchAudioWithChars(words, wordlen, GetHills, matchresult);
    
    
    FormatFile FF;
    FF.FrameCount=PowerofFrame.Count();
    FF.WordsCutCount=matchresult.Count();
    
    FF.WordsCut=new int[FF.WordsCutCount*2];
    FF.FrameCut=new int[FF.WordsCutCount*2];
    
    for(int i=0;i<FF.WordsCutCount;i++)
    {
        FF.WordsCut[i*2]=matchresult[i].hillUnitAStar;
        FF.WordsCut[i*2+1]=matchresult[i].hillUnitAEnd;
        FF.FrameCut[i*2]=matchresult[i].hillUnitBStar;
        FF.FrameCut[i*2+1]=matchresult[i].hillUnitBEnd;
    }
    
    FF.MFCC=new double*[FF.FrameCount];
    FF.PowerfulList=new double[FF.FrameCount];
    for(int i=0; i< FF.FrameCount; i++)
    {
        FF.PowerfulList[i]=PowerofFrame[i];
        FF.MFCC[i]=new double[24];
        for(int j=0; j< 24; j++)
        {
            FF.MFCC[i][j]=dtm.Data[i][j];
        }
    }
    FF.wavelen=len;
    FF.wavfile=new Byte[len];
    for (int i=0; i<len; i++)
    {
        FF.wavfile[i]=FileBytes[i];
    }
    
    
    return FF;
}

CRuslt GetPointResult(Byte* F_File,int FormatFileLen, Byte* waveFileofUser, int WaveFileLen)
{
    FormatFile simple=GetWavFile(F_File, FormatFileLen);
    FormatFile user=ParseFormatFile(waveFileofUser, WaveFileLen, simple.Words, simple.wordLen);
    
    List<int> dd2(simple.WordsCut,simple.WordsCutCount*2);
    
    int* wordmatchresult;
    int len= wordMatch(simple.FrameCut, simple.WordsCut, simple.WordsCutCount, user.FrameCut, user.WordsCut, user.WordsCutCount, wordmatchresult);
    
    
    List<int> dd(wordmatchresult,len);
    CRuslt Presult;
    
    Presult.WordCount=len/4;
    Presult.WordsMatchedPoint=new int[Presult.WordCount];
    Presult.WordSplitCount=new int[Presult.WordCount];
    
    for(int i=0; i< simple.WordsCutCount*2; i=i+2)
    {
        Presult.WordSplitCount[i/2]=simple.WordsCut[i+1];
    }
    
    double sum=0;
    for(int i=0; i<len; i=i+4)
    {
        double p= GetMFCCPoint(simple.MFCC, wordmatchresult[i], wordmatchresult[i+1], user.MFCC, wordmatchresult[i+2], wordmatchresult[i+3]);
        Presult.WordsMatchedPoint[i/4]=p;
        printf("%f \n",p);
        sum+=p;
    }
    
    Presult.IsMatched=true;
    Presult.point=sum;
    
    return CRuslt();
}

Byte* CreatedFormatFile(Byte* waveFileByte, char words[],int wordlen, int wavefileLength, int &OutLength)
{
    FormatFile FF=ParseFormatFile(waveFileByte, wavefileLength, words,wordlen);
    FF.version=1.0;
    FF.Words=words;
    
    FF.wordLen=wordlen;
    
    List<Byte> output= CreateFormatFile(FF);
    OutLength=output.Count();
    
    printf("Framework Version: %f", 1.0);
    return GetArrayList(output);
}
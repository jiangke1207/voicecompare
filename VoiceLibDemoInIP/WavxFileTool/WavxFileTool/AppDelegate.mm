//
//  AppDelegate.m
//  FromDataToFile
//
//  Created by jiangqc on 14/10/28.
//  Copyright (c) 2014年 jiangqc. All rights reserved.
//

#import "AppDelegate.h"

#import <VoiceLibForMac/MainEntry.h>
#import <VoiceLibForMac/Structions.h>

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    
    _fileArray = [[NSMutableArray alloc] init];
    
    _selectFilePath = nil;
}


- (IBAction)selectFileBtnAction:(NSButton *)sender {
    [self OpenFileDialog];
}

- (IBAction)changeFileToByte:(NSButton *)sender {
    if (_textView.string != nil && ![_textView.string isEqualToString:@""]) {
        NSLog(@"you write:%@",_textView.string);
        NSLog(@"you select file path :%@",_selectFilePath);
        
        NSData *selectFileData = [NSData dataWithContentsOfFile:_selectFilePath];
        NSLog(@"you select file length :%li",selectFileData.length);
        
        Byte bytelist[selectFileData.length];
        
        [selectFileData getBytes:bytelist];
        
        char* ddd=new char[2];
        ddd[0]='1';
        ddd[1]='1';
        
        int len=0;
        Byte* outlist= CreatedFormatFile(bytelist, ddd,2, selectFileData.length, len);
        
        int ddddd=0;
    }
}


- (void)OpenFileDialog
{
    // Create the File Open Dialog class.
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    
    // Enable the selection of files in the dialog.
    [openDlg setCanChooseFiles:YES];
    
    // Enable the selection of directories in the dialog.
    [openDlg setCanChooseDirectories:NO];
    
    [openDlg setAllowedFileTypes:[NSArray arrayWithObjects:@"wav", nil]];
    
    [openDlg setAllowsMultipleSelection:NO];
    
    // Display the dialog.  If the OK button was pressed,
    // process the files.
    if ( [openDlg runModal] == NSOKButton )
    {
        // Get an array containing the full filenames of all
        // files and directories selected.
        NSArray* files = [openDlg URLs];
        
        _selectFilePath = [files.lastObject path];
        
        
        /*
        if (_fileArray == nil) {
            _fileArray = [[NSMutableArray alloc] init];
        }
        
        [_fileArray removeAllObjects];
        
        
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        
        // Loop through all the files and process them.
        for(int i = 0; i < [files count]; i++ )
        {
            NSString* fileName = [[files objectAtIndex:i] path];
            NSLog(@"fileName = %@",fileName);
            // Do something with the filename.

            NSError *error = nil;
            NSArray *fileList = [[NSArray alloc] init];
            //fileList便是包含有该文件夹下所有文件的文件名及文件夹名的数组
            fileList = [fileManager subpathsOfDirectoryAtPath:fileName error:&error];
            
            for (NSString *pathStr in fileList) {
                if ([[pathStr componentsSeparatedByString:@"."].lastObject isEqualToString:@"png"]) {
                    NSString *fileUrlStr = [NSString stringWithFormat:@"%@/%@",fileName, pathStr];
                    [_fileArray addObject:fileUrlStr];
                }
            }
            
            NSLog(@"_fileArray = %@",_fileArray);
        } 
        */
    } 
}

@end

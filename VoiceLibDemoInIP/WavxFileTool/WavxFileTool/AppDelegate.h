//
//  AppDelegate.h
//  FromDataToFile
//
//  Created by jiangqc on 14/10/28.
//  Copyright (c) 2014年 jiangqc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>
{
    NSMutableArray *_fileArray;
    
    NSString *_selectFilePath;
}

@property (assign) IBOutlet NSWindow *window;
@property (unsafe_unretained) IBOutlet NSTextView *textView;


@end

function Rout=FFTPowerDB(fftresult)
    Rout=20*log(fftresult);
end
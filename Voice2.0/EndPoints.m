function Result=EndPoints(VX,EntropyList,ZeroPassList,PowerList,WindowLen,FS)

Result=zeros(2,100);
[x,y]=size(PowerList);
newentropylist=zeros(1,y);
for iii=1:y
    newentropylist(iii)=1-EntropyList(iii);
end
newline=PowerList.*newentropylist;

%index=find(newline>0.001);

%Result=newline;
%sound(VX(x*(WindowLen/2):(y+1)*(WindowLen/2)));

index=1;
bt=0;
for ii=1:y
    if PowerList(ii)>0.02
        if bt==0
            Result(1,index)=ii;
            bt=1;
        else
            continue;
        end
    else
        if bt==0
            continue;
        else
            Result(2,index)=ii;
            bt=0;
            index=index+1;
        end
    end
end

Result=Result(:,1:index-1);

end
function R=Guiyi(VX)

[row,col]=size(VX);

Vmax=max(VX);

for ii=1:row
    for jj=1:col
        if VX(ii,jj)>0
            VX(ii,jj)=abs(VX(ii,jj))/Vmax;
        else
            VX(ii,jj)=-abs(VX(ii,jj))/Vmax;
        end
    end
end

R=VX;

end
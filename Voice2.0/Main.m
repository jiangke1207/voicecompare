clear;
[VX,FS]=wavread('111.wav');
[a,b,p1]=VoiceOperator(VX,FS,1);
y1=mainmfcc2(VX,FS,2,56,73);

result=zeros(1,30);
for i=1:20
    path=strcat(num2str(i),'.wav');
    disp(path);
    
    [VX2,FS2]=wavread(path);
    [c,d,p2]=VoiceOperator(VX2,FS2,3);
    y2=mainmfcc2(VX2,FS2,4,45,62);

    result(1,i)=FFTPowerCompare(y1(a(1,1):a(2,b),:),y2(c(1,1):c(2,d),:),p1(a(1,1):a(2,b)),p2(c(1,1):c(2,d)),a,c);
end

a=0;

%[m1,t]=VoiceOperator(VX,FS,1);

%[VX2,FS2]=wavread('1.wav');

%[m2,t2]=VoiceOperator(VX2,FS2,3);

%FFTPowerCompare(t,t2);
%GetPoint(m1,m2);
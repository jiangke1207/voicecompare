function D=OShiDe(m1,m2)
    D=0;
    for ii=12:24
        D=D+(m2(ii)-m1(ii))^2;
    end
    
    D=D^0.5;
end
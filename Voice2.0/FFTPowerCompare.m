function result=FFTPowerCompare(m1,m2,p1,p2,e1,e2)

figure(5);

[x1,y1]=size(m1);
[x2,y2]=size(m2);

for i=1:size(m1(:,1))
    F1(i)= sqrt(sum(m1(i,:).*m1(i,:)));
end

for i=1:size(m2(:,1))
    F2(i)= sqrt(sum(m2(i,:).*m2(i,:)));
end

result2=zeros(x1,x2);
for x=1:x1
        for y=1:x2
            result2(x,y)=abs(F1(x)-F2(y));
        end
end

fanwei=2;

c(1,1)=1;
c(2,1)=1;
c(3,1)=result2(1,1);
Index=2;
x=1;
y=1;
while(1)
    minindexI=-1;
    minindexR=-1;
    minvalue=99999;
    for i=x:x+fanwei-1
        for r=y:y+fanwei-1
            if i<=x1 && r <=x2 && (i~=x && r~=y)
                if minvalue > result2(i,r)
                    minvalue=result2(i,r);
                    minindexI=i;
                    minindexR=r;
                end
            end
        end
    end
    
    if minindexI==-1
    break;
    end
    
    if c(1,Index-1)==minindexI || c(2,Index-1)==minindexR
        if c(3,Index-1)>minvalue
            c(1,Index-1)=minindexI;
            c(2,Index-1)=minindexR;
            c(3,Index-1)=minvalue;
        end
    else
        c(1,Index)=minindexI;
        c(2,Index)=minindexR;
        c(3,Index)=minvalue;
        Index=Index+1;
    end
    
    x=minindexI;
    y=minindexR;
end
[aaa,bbb]=size(c);


result=1/sum(c(3,:));
pipeilv=(bbb*2)/(x1+x2);
result=pipeilv*result;

subplot(3,1,1);  

plot(c(3,:));



subplot(3,1,2);
plot(F1);
hold on;
plot(F2,'r');
hold off;
end


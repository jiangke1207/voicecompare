function freq=getTriFilterBankPrm(fs, filterNum)
% getTriFilterBankPrm: Get the parameters of the triangular band-pass filter bank used in computing MFCC
%	Usage: freq=getTriFilterBankPrm(fs, filterNum, plotOpt)
%获得 三角带通滤波器
%	Roger Jang, 20060417

fLow=0;
fHigh=fs/2;
% Compute the frequencies of the triangular band-pass filters
for i=1:filterNum+2
	f(i)=mel2linFreq(lin2melFreq(fLow)+(i-1)*(lin2melFreq(fHigh)-lin2melFreq(fLow))/(filterNum+1));
end
freq=[];
for i=1:filterNum
	freq=[[f(i); f(i+1); f(i+2)], freq];
end

end

function melFreq=lin2melFreq(linFreq)
%lin2melFreq: Linear frequency to mel frequency conversion
% 频率＝》mel频率
%	Roger Jang, 20020502

melFreq=1125*log(1+linFreq/700); 

end

function linFreq=mel2linFreq(melFreq)
%mel2linFreq: Mel frequency to linear frequency conversoin
% mel频率＝》频率
%	Roger Jang, 20020502

linFreq=700*(exp(melFreq/1125)-1);	

end

function Result=FindHill(newline)
[x,y]=size(newline);

index=1;
lr=-1;
for i=2:y
    if(i==2)
        lr=GetF(newline(i),newline(i-1));
        Result(1,index)=i-1;
        continue;
    end

    r=GetF(newline(i),newline(i-1));
    if(r~=lr)    
        Result(2,index)=i-1;
        Result(3,index)=Result(2,index)-Result(1,index);
        Result(4,index)=r;
        tsum=sum(newline(Result(1,index):Result(2,index))) ;
        Result(5,index)=tsum;
        if( tsum>0.0001)    
        index=index+1;
        end

        Result(1,index)=i-1;   
    end
    
    if i==y
        Result(2,index)=i-1;
        Result(3,index)=Result(2,index)-Result(1,index);
        Result(4,index)=-r;
        tsum=sum(newline(Result(1,index):Result(2,index))) ;
        Result(5,index)=tsum;
        if( tsum>0.0001)    
        index=index+1;
        end
    end
    
    lr=GetF(newline(i),newline(i-1));
end

lindex=1;
index=index-1;

i=0;
for r=2:index
    i=i+2;
    
    if i>index
        break;
    end
    
    if Result(4,i)+Result(4,i-1)==0 %& Result(3,i)+Result(3,i-1) >4
        NR(1,lindex)=Result(1,i-1);
        NR(2,lindex)=Result(2,i);
        
        NR(3,lindex)=(Result(2,i)-Result(1,i-1));
        NR(4,lindex)=sum(newline(NR(1,lindex):NR(2,lindex)))/NR(3,lindex);
        NR(5,lindex)=NR(3,lindex)/2+Result(1,i-1);
        NR(6,lindex)=NR(4,lindex)/NR(3,lindex);
        %if NR(4,lindex) >0.01
        lindex=lindex+1;
        %end
    end
end

lindex=lindex-1;

Result=NR;
end

function result=GetF(x,y)
    if(x-y>=0)
        result=1;
    end
    
    if(x-y<0)
        result=-1;
    end
end
function   [endarray,y,PowerInWindow]=VoiceOperator(VX,FS,Fcount)
mfccNum=12;
filterNum=20;


%预加重
VX=PreEmphasis(VX);


%加窗
VXW=AddWindow(VX,256);

%功率谱熵
EntropyList=Guiyi(GetEntropyList(VXW,FS));

%短时能量（in frame）
PowerInWindow=Guiyi(GetWindowpower(VXW));

%Passzero
PassZeroArray=Guiyi(GetZeroPass(VXW));



%endpoint
endarray=EndPoints2(VX,EntropyList,PassZeroArray,PowerInWindow,256,FS);

OrgData(EntropyList,PowerInWindow,Fcount,endarray);

[x,y]=size(endarray);
%VXW=VXW(endarray(1,1):endarray(1,2),:);



end
function OrgData(EntropyList,PowerList, figureno, endpoints)
[x,y]=size(PowerList);
newentropylist=zeros(1,y);
for iii=1:y
    newentropylist(iii)=1-EntropyList(iii);
end
newline=PowerList.*newentropylist;
%newline=EntropyList;


figure(figureno);
[a,b]=size(endpoints);
subplot(3,1,1);

plot(newline,'r');
hold on;
for i=1:b
plot([endpoints(1,i),endpoints(1,i)],[min(newline),max(newline)]);
plot([endpoints(2,i),endpoints(2,i)],[min(newline),max(newline)]);
end
hold off;

subplot(3,1,2);
plot(EntropyList);

subplot(3,1,3);
plot(PowerList);

%subplot(3,1,2);
%plot(EntropyList);
%grid on;
%subplot(3,1,3);
%plot(PowerList);
%grid on;
end
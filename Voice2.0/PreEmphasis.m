function outvalue=PreEmphasis(input)

Factor=0.98;

inputLL=length(input);

acc=zeros(0,inputLL);

acc(1) = input(1) - Factor*input(1);

for ii=2:inputLL
    acc(ii)=input(ii)-Factor*input(ii-1);
end

outvalue=acc;
end
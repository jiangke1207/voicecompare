function [fftarray,level]=GetFFT(input)
    
    level=floor(log2(length(input)));
    if 2^level<length(input)
        level=level+1;
    end

    fftarray=zeros(2,2^level);
    for ii=1:length(input)
        fftarray(1,ii)=input(ii);
    end
    
    ta=0;
    tb=0;
    ua=0;
    ub=0;
    wa=0;
    wb=0;
    
    n=1;
    for ii=1:level;
        n=n*2;
    end
    
    nv2=n/2;
    nm1=n-1;
    j=1;
    
    for ii=1:nm1;
        if ii< j
            ta=fftarray(1,ii-1 +1);
            tb=fftarray(2,ii-1  +1);
            fftarray(1,ii-1  +1)=fftarray(1,j-1  +1);
            fftarray(2,ii-1  +1)=fftarray(2,j-1  +1);
            fftarray(1,j-1  +1)=ta;
            fftarray(2,j-1  +1)=tb;
        end
        
        k=nv2;
        
        while k<j
            j=j-k;
            k=k/2;
        end
        
        j=j+k;
    end
    
    le=1;
    
    for l=1:level
        le=le*2;
        le1=le/2;
        
        ua=1;
        ub=0;
        wa=cos(pi/le1);
        wb=-sin(pi/le1);
        
        for j=1:le1
            i=j;
            for it=1:10000, i=j+ (it-1)*le;
                if i>n
                    break;
                end
                ip=i+le1;
                ta=fftarray(1,ip-1 +1)* ua- fftarray(2,ip-1 +1)* ub;
                tb=fftarray(1,ip-1 +1)* ub + fftarray(2,ip-1 +1)* ua;
                fftarray(1,ip-1 +1)=fftarray(1,i-1 +1)-ta;
                fftarray(2,ip-1 +1)=fftarray(2,i-1 +1)-tb;
                fftarray(1,i-1 +1)=ta+fftarray(1,i-1 +1);
                fftarray(2,i-1 +1)=tb+fftarray(2,i-1 +1);
            end
            
            ta=ua*wa-ub*wb;
            tb=ub*wa+ua*wb;
            ua=ta;
            ub=tb;
        end
    end
    
    
end
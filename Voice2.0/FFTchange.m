function [FFTresult,FreqArray]=FFTchange(VectorInput,FS)
[row,col]=size(VectorInput);
FFTresult=zeros(1,col/2+1);

freqStep = FS/col;
FreqArray=freqStep*(0:col/2);

for ii=1:row
    [B,level]=GetFFT(VectorInput(ii,:));

    B(:,2:length(B)-1)=2*B(:,2:length(B)-1);
    for jj=1:col/2+1
        FFTresult(jj)=FFTresult(jj)+ sqrt(B(1,jj)*B(1,jj)+B(2,jj)*B(2,jj));
    end
    
    %disp(FFTresult(1));
end
end
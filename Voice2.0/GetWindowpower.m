function powF=GetWindowpower(VXW)

[row,col]=size(VXW);

powF=zeros(1,row);

for ii=1:row
   for jj=1:col
    pp=VXW(ii,jj)^2;
    powF(ii)=powF(ii)+pp;
   end
end

end
function EntropyList=GetEntropyList(VX,FS)
childpartcount=10;

[row,col]=size(VX);
EntropyList=zeros(1,row);

freqStep = FS/col;

for ii=1:row
    [B,level]=GetFFT(VX(ii,:));
    BP=zeros(1,childpartcount);
    sumP=0;
    for jj=1:col/2+1
        freq=jj*freqStep;
        if freq >250 && freq < 6000
            partindex= floor((freq -250)/(575))+1;
            cp=(B(1,jj)*B(1,jj)+B(2,jj)*B(2,jj))^0.5;
            BP(partindex)=BP(partindex)+ cp;
            sumP=sumP+cp;
        end
    end
    
    pl=zeros(1,childpartcount);
    for jj=1:childpartcount
        if(sumP~=0)
            pl(jj)=BP(jj)/sumP;
        else
            pl(jj)=0;
        end
        
    end
    
    for jj=1:childpartcount
        if pl(jj)==0
            continue;
        end
        EntropyList(ii)=EntropyList(ii)- pl(jj)*log(pl(jj));
    end    
end

for ii=2:row
    EntropyList(ii)=0.94*EntropyList(ii-1)+(1-0.94)*EntropyList(ii);
end

end
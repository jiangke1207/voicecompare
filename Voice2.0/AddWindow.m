function Rout=AddWindow(VX,WindowLength)
VLength=length(VX);
FrameCount= floor(VLength/(WindowLength/2))+1;

windowFu= hamming(WindowLength);
DataInWindow=zeros(FrameCount,WindowLength);

for ii=1:FrameCount, sp= (ii-1)*(WindowLength/2);
    for jj=1:WindowLength;
        if sp+jj > VLength
            break;
        end
        DataInWindow(ii,jj)=VX(sp+jj)*windowFu(jj);
    end
end

Rout=DataInWindow;
end
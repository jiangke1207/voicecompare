function powF=GetZeroPass(VXW)

[row,col]=size(VXW);

powF=zeros(1,row);

for jj=1:row
    input=VXW(jj,:);
    ZeroPassArray=0;
    for ii=1:col-1
        ZeroPassArray=ZeroPassArray+abs(sign(input(ii+1))-sign(input(ii)));
    end
    ZeroPassArray=ZeroPassArray/2;
    powF(jj)=ZeroPassArray;
end

    
end
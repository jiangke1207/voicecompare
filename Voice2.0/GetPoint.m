function Point=GetPoint(MFCC1,MFCC2)

    [x1,y1]=size(MFCC1);
    [x2,y2]=size(MFCC2);
    
    for i=1:y1
        disp(GetSingleMFCC(MFCC1(:,i),MFCC2(:,i)));
    end

end


function Ssingle=GetSingleMFCC(mfc1,mfc2)

    sum=0;
    for i=2:12
        
        sum=sum+ abs(sign(mfc1(i)-mfc1(i-1)) -sign(mfc2(i)-mfc2(i-1))) ;
    end
    Ssingle=sum;
end
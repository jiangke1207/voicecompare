function Result=EndPoints2(VX,EntropyList,ZeroPassList,PowerList,WindowLen,FS)

Result=zeros(5,200);
[x,y]=size(PowerList);
newentropylist=zeros(1,y);
for iii=1:y
    newentropylist(iii)=1-EntropyList(iii);
end
newline=PowerList.*newentropylist;

for jj=2: y-1
    %newline(jj)=(newline(jj-1)+newline(jj+1))/2;
end


index=1;
lr=-1;
for i=2:y
    if i==196
    end
    disp(i);
    if(i==2)
        lr=GetF(newline(i),newline(i-1));
        Result(1,index)=i-1;
        continue;
    end

    r=GetF(newline(i),newline(i-1));
    if(r~=lr)    
        Result(2,index)=i-1;
        Result(3,index)=Result(2,index)-Result(1,index);
        Result(4,index)=r;
        tsum=sum(newline(Result(1,index):Result(2,index))) ;
        Result(5,index)=tsum;
        if( tsum>0.0001)    
        index=index+1;
        end

        Result(1,index)=i-1;   
    end
    
    if i==y
        Result(2,index)=i-1;
        Result(3,index)=Result(2,index)-Result(1,index);
        Result(4,index)=-r;
        tsum=sum(newline(Result(1,index):Result(2,index))) ;
        Result(5,index)=tsum;
        if( tsum>0.0001)    
        index=index+1;
        end
    end
    
    lr=GetF(newline(i),newline(i-1));
end

lindex=1;

i=0;
for r=2:index
    i=i+2;
    
    if i>index
        break;
    end
    
    if Result(4,i)+Result(4,i-1)==0 %& Result(3,i)+Result(3,i-1) >4
        NR(1,lindex)=Result(1,i-1);
        NR(2,lindex)=Result(2,i);
        maxx=max(newline(Result(1,i-1):Result(2,i-1)));
        minx=min(newline(Result(1,i-1):Result(2,i-1)));
        max2=max(newline(Result(1,i):Result(2,i)));
        min2=min(newline(Result(1,i):Result(2,i)));
        baomandu=GetF(minx,min2)*max(minx,min2);
        NR(3,lindex)=(((maxx+minx)/2)-((max2+min2)/2));
        NR(4,lindex)=sum(newline(NR(1,lindex):NR(2,lindex)));
        NR(5,lindex)=baomandu;
        if NR(4,lindex) >0.01
        lindex=lindex+1;
        end
    end
end

lindex=lindex-1;

ir=1;
while 1
    %if NR(3,ir+1) * NR(3,ir)<0 && abs(NR(3,ir+1) + NR(3,ir))<abs(NR(3,ir+1))&& abs(NR(3,ir+1) + NR(3,ir))<abs(NR(3,ir)) && NR(2,ir)==NR(1,ir+1) && abs(NR(3,ir))>0.002
    
    if abs(NR(3,ir))>0.01 && NR(2,ir)==NR(1,ir+1)
    
        NR(1,ir)=NR(1,ir);
        NR(2,ir)=NR(2,ir+1);
        NR(3,ir)=NR(3,ir+1) + NR(3,ir);
        NR(:,ir+1)=[];
        ir=ir-1;
        lindex=lindex-1;
    end
    
    ir=ir+1;
    if ir+1 > lindex
        break;
    end
end


Result=NR(:,1:lindex);

end


function result2=GetF(x,y)
    if(x-y>=0)
        result2=1;
    end
    
    if(x-y<0)
        result2=-1;
    end
end
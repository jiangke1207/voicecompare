//
//  OperatorLib.h
//  VoiceSum
//
//  Created by 姜珂 on 14-10-8.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#ifndef __VoiceSum__OperatorLib__
#define __VoiceSum__OperatorLib__

#include <stdio.h>
#include "Generator.h"
#include <math.h>

class OLib
{
public:
    OLib();
    ArrayList PreEmphasis(ArrayList org_data);
    VectorList AddWindow(ArrayList input, int winLen, int& WinCount);
    
    COMPLEX** FFTWindowfulArray(VectorList windowfulinput,int fs, ArrayList fftpower, ArrayList freqlist);
    COMPLEX * FFT(double* input,int inputlength);
    ArrayList GetEntropy(COMPLEX** fftfulinput,int FS,int windowCount,int windowLength);
    double* GetZeroPass(double** input, int windowCount,int windowLength);
    double* GetHamming(int len);
    int sign(double x);
    double linetomelFreq(double lineFreq);
    double meltolineFreq(double melFreq);
    VectorList getTriFilterBankPrm(int fs, int filterNum);
    double * Guiyi(double *inputlist, int count);
    FrameRange* SplitEndpoint(ArrayList Entropylist, int entropylen, int windowLen, int FS, int &resultSize);
    ArrayList getFilterFactor(ArrayList fftpowerlist, ArrayList freqlist, VectorList triFilterbankPrmlist, int filterNum);
    ArrayList Trimf(ArrayList x, ArrayList prm);
    
    ArrayList GetMfcc(ArrayList filterFactorArray, int mfccNum, int filterNum);
    
private:
    double preEmphasisFactor=0.98;
    double pi=3.1415926;
    int EntropypartCount=10;
};

#endif /* defined(__VoiceSum__OperatorLib__) */

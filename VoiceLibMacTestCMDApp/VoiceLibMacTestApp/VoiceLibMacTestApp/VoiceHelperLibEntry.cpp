//
//  VoiceHelperLibEntry.cpp
//  VoiceSum
//
//  Created by 姜珂 on 14-10-8.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#include "VoiceHelperLibEntry.h"
#include "OperatorLib.h"
ArrayList VoiceLibEntry::GetMFCC(double* input, int len, int FS)
{
    ArrayList Org_Data(input,len);
    
    OLib lib;
    
    ArrayList PreEmphasis_Data= lib.PreEmphasis(Org_Data);
    
    int WindowCount=0;
    VectorList AddWindow_Data=lib.AddWindow(PreEmphasis_Data, 256, WindowCount);
    
    ArrayList powerforfft(AddWindow_Data.Len2/2+1);
    ArrayList fftfreq(AddWindow_Data.Len2/2+1);
    
    COMPLEX** FFTPerWindow=lib.FFTWindowfulArray(AddWindow_Data, FS, powerforfft, fftfreq);
    
    ArrayList Entropylist=lib.GetEntropy(FFTPerWindow, FS, AddWindow_Data.Len1, AddWindow_Data.Len2);
    
    int wordcount=0;
    FrameRange* words=lib.SplitEndpoint(Entropylist, AddWindow_Data.Len1, 256 , FS,  wordcount);
    
    VectorList trifilterbankPrm=lib.getTriFilterBankPrm(FS, 20);
    
    ArrayList filterfactor= lib.getFilterFactor(powerforfft,  fftfreq, trifilterbankPrm, 20);
    
    ArrayList mfcc=lib.GetMfcc(filterfactor, 12, 20);
    
    printf("input Org_Data value: %f\n",Org_Data.SumValue());
    printf("input PreEmphasis_Data value: %f\n",PreEmphasis_Data.SumValue());
    printf("input AddWindow_Data value: %f\n",AddWindow_Data.SumValue());
    printf("input Entropylist value: %f\n",Entropylist.SumValue());
    printf("input powerforfft value: %f\n",powerforfft.SumValue());
    printf("input fftfreq value: %f\n",fftfreq.SumValue());
    printf("input trifilterbankPrm value: %f\n",trifilterbankPrm.SumValue());
    printf("input filterfactor value: %f\n",filterfactor.SumValue());
    printf("input mfcc value: %f\n",mfcc.SumValue());

    
    return mfcc;
}
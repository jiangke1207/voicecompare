//
//  OperatorLib.cpp
//  VoiceSum
//
//  Created by 姜珂 on 14-10-8.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#include "OperatorLib.h"


OLib::OLib():preEmphasisFactor(0.98)
{
    pi=3.1415926;
}

ArrayList OLib::PreEmphasis(ArrayList org_data)
{
    int len=org_data.Count();
    double* input=org_data.Data;
    
    double* result=new double[len];
    
    result[0]=input[0]- preEmphasisFactor*input[0];
    
    for(int i=1; i< len; i++)
    {
        result[i]=input[i]-preEmphasisFactor* input[i-1];
    }
    
    return ArrayList(result,len);
}

VectorList OLib::AddWindow(ArrayList data, int winLen, int& WinCount)
{
    int len=data.Count();
    double* input=data.Data;
    
    int windowsCount=floor(len / (winLen/2))+1;
    
    double* windowFunction= this->GetHamming(winLen);
    
    double ** result= new double*[windowsCount];
    
    for(int i=0;i<windowsCount;i++)
    {
        result[i]=new double[winLen];
        for(int j=0;j<winLen; j++)
        {
            result[i][j]=0;
        }
    }
    
    for(int ii=0; ii<windowsCount; ii++)
    {
        int sp=ii*(winLen/2);
        for(int jj=0; jj<winLen; jj++)
        {
            if(sp+jj> len)
            {
                break;
            }
            
            result[ii][jj]=input[jj+sp]* windowFunction[jj];
        }
    }
    
    WinCount=windowsCount;
    
    delete[] windowFunction;
    
    return VectorList(result,windowsCount,winLen);
}

COMPLEX** OLib::FFTWindowfulArray(VectorList windowfulinput,int fs, ArrayList fftpower, ArrayList freqlist)
{
    COMPLEX** result=new COMPLEX*[windowfulinput.Len1];
    int windowCount=windowfulinput.Len1;
    
    
    
    for(int i=0; i< windowCount; i++)
    {
        COMPLEX* temp=this->FFT(windowfulinput.Data[i], windowfulinput.Len2);
        
        result[i]=temp;



        for( int j=0; j< windowfulinput.Len2/2+1; j++)
        {
            
            if(j==0){
                fftpower.Data[j]+=sqrt(temp[j].real*temp[j].real+temp[j].image*temp[j].image);
            }else
            {
                fftpower.Data[j]+=sqrt(temp[j].real*temp[j].real*4+temp[j].image*temp[j].image*4);
            }
            
        }
        
    }
    
    for( int i=0; i<windowfulinput.Len2/2+1;i++)
    {
        fftpower.Data[i]=20*log(fftpower.Data[i]);
    }
    
    double freqStep=fs/(double)windowfulinput.Len2;
    
    for(int j=0; j< windowfulinput.Len2/2+1; j++)
    {
        freqlist.Data[j]=freqStep* j;
    }
    
    return result;
}

double* OLib::GetHamming(int len)
{
    double* hamming=new double[len];
    
    for(int i=0; i<len; i++)
    {
        hamming[i]=0.54 -0.46* cos(2*pi*i/(len-1));
    }
    
    return hamming;
}

COMPLEX * OLib::FFT(double* input,int inputlength)
{
    int level=floor(log2(inputlength));
    
    if (pow(2, level)< inputlength)
        level++;
    
    int colcount=pow(2, level);
    
    COMPLEX* result=new COMPLEX[colcount];
    
    for (int i=0; i< inputlength; i++)
    {
        result[i].real=input[i];
        result[i].image=0;
    }
    
    COMPLEX u,w,t;
    
    int n , i , nv2 , j , k , le , l , le1 , ip , nm1 ;
    
    n = 1;
    for(i=0; i<level; i++)
        n = n*2 ;
    
    nv2 = n / 2 ;
    nm1 = n - 1 ;
    j = 1 ;
    
    for (i = 1 ; i <= nm1 ; i ++)
    {
        if (i < j)
        {
            t.real = result[i - 1].real ;
            t.image = result[i - 1].image ;
            result[i - 1].real = result[j - 1].real ;
            result[i - 1].image = result[j - 1].image ;
            result[j - 1].real = t.real ;
            result[j - 1].image = t.image ;
        }
        
        k = nv2 ;
        
        while (k < j)
        {
            j -= k ;
            k /= 2 ;
        }
        j += k ;
    }
    
    le = 1 ;
    for (l= 1 ; l <= level ; l ++)
    {
        le =le * 2 ;
        le1 = le / 2 ;
        u.real = 1.0f ;
        u.image = 0.0f ;
        w.real =  cos(pi / le1) ;
        w.image = -sin(pi / le1) ;
        
        for (j = 1 ; j <= le1 ; j ++)
        {
            i=j;
            for (int it = 1 ; it <= 10000 ; it ++)
            {
                i=j+(it-1)*le;
                if(i>n)break;
                ip = i + le1 ;
                t.real = result[ip - 1].real * u.real - result[ip - 1].image * u.image ;
                t.image = result[ip - 1].real * u.image + result[ip - 1].image * u.real ;
                result[ip - 1].real = result[i - 1].real - t.real ;
                result[ip - 1].image = result[i - 1].image - t.image ;
                result[i - 1].real = t.real + result[i - 1].real ;
                result[i - 1].image = t.image + result[i - 1].image ;
            }
            
            t.real = u.real * w.real - u.image * w.image ;
            t.image = u.image * w.real + u.real * w.image ;
            u.real = t.real ;
            u.image = t.image ;
        }
    }
    
    return result;
}

ArrayList OLib::GetEntropy(COMPLEX** fftfulinput,int FS,int windowCount,int windowLength)
{
    double* result=new double[windowCount];
    
    double freqStep=FS/(double)windowLength;
    
    for(int i=0; i<windowCount; i++)
    {
        COMPLEX* fftcurrent= fftfulinput[i];
        double sumP=0;
        double* BP=new double[EntropypartCount];
        
        
        for(int j=0;j<EntropypartCount;j++)
            BP[j]=0;
        
        for(int j=1; j<= (windowLength/2+1);j++)
        {
            double freq=j*freqStep;
            if(freq >250 && freq < 6000)
            {
                int pindex=floor((freq-250)/575);
                double cp= sqrt(fftcurrent[j-1].real*fftcurrent[j-1].real+ fftcurrent[j-1].image*fftcurrent[j-1].image);
                BP[pindex]+=cp;
                sumP+=cp;
            }
        }
        
        
        for(int j=0; j< EntropypartCount; j++)
        {
            BP[j]=BP[j]/sumP;
        }
        
        for (int j=0; j<EntropypartCount; j++)
        {
            if(BP[j]==0) continue;
            result[i]-=BP[j]*log(BP[j]);
            
        }
        
        delete[] BP;
        delete fftcurrent;
    }
    
    for(int a=1;a<windowCount;a++){
        result[a]=0.94*result[a-1]+(1-0.94)*result[a];
    }
    
    result= this->Guiyi(result, windowCount);
    
    return ArrayList(result,windowCount);
}

double* OLib::GetZeroPass(double** input, int windowCount,int windowLength)
{
    double* result=new double[windowCount];
    
    for(int i=0; i<windowCount;i++)
    {
        double* cinput=input[i];
        double zeropassarray=0;
        for(int j=1;j<windowLength-1;j++)
        {
            zeropassarray+=fabs(this->sign(cinput[j])-this->sign(cinput[j-1]));
        }
        result[i]=zeropassarray/2;

    }
    return result;
}

int OLib::sign(double x)
{
    if(x>0)
        return 1;
    else
        return -1;
    
}

FrameRange* OLib::SplitEndpoint(ArrayList Entropylist, int entropylen, int windowLen, int FS, int &resultSize)
{
    int resultcount=0;
    FrameRange* result=new FrameRange[resultcount];
    
    int index=0;
    int pstart=-1;
    int pend=-1;
    
    while (index<entropylen) {
        if(Entropylist.Data[index]<0.97)
        {
            if(pstart<0)
            {
                pstart=index;
            }
            else
            {
                pend=index;
            }
        }
        else
        {
            if(pend>0 && (pend-pstart)*(windowLen/2)/FS>0.2)
            {
                FrameRange* nr=new FrameRange[resultcount+1];
                for(int i=0; i<resultcount;i++)
                {
                    nr[i].start=result[i].start;
                    nr[i].end=result[i].end;
                }
                
                nr[resultcount].start=pstart;
                nr[resultcount].end=pend;
                resultcount++;
                delete result;
                result=nr;
            }
            pstart=-1;
            pend=-1;
        }
        
        index++;
    }
    
    resultSize=resultcount;
    
    return result;
}

double OLib::linetomelFreq(double lineFreq)
{
    return 1125*log(1+lineFreq/700);
}

double OLib::meltolineFreq(double melFreq)
{
    return 700*(exp(melFreq/1125)-1);
}

VectorList OLib::getTriFilterBankPrm(int fs, int filterNum)
{
    int Flow=0;
    int Fhig=fs/2;
    
    double* f=new double[filterNum+1];
    
    for(int i=0; i< filterNum+2; i++)
    {
        f[i]=meltolineFreq(linetomelFreq(Flow)+i*(linetomelFreq(Fhig)-linetomelFreq(Flow))/(filterNum+1));
    }
    
    double** result=new double*[3];
    for( int i=0;i<3;i++)
        result[i]=new double[filterNum];
    
    for(int i=filterNum-1;i>=0;i--)
    {
        result[0][i]=f[filterNum-1-i];
        result[1][i]=f[filterNum-1-i +1];
        result[2][i]=f[filterNum-1-i +2];
    }
    
    delete[] f;
    return VectorList(result,3,filterNum);
}

double * OLib::Guiyi(double *inputlist, int count)
{
    double max=0;
    for(int i=0; i< count; i++)
    {
        if((inputlist[i]>=0 && inputlist[i]>max)||(inputlist[i]<0 && -inputlist[i]>max))
        {
            max=inputlist[i];
            if(max<0)
                max=-max;
        }
    }
    
    for(int i=0;i<count;i++)
    {
        
        inputlist[i]=inputlist[i]/max;
    }
    
    return inputlist;
}

ArrayList OLib::getFilterFactor(ArrayList fftpowerlist, ArrayList freqlist, VectorList triFilterbankPrmlist, int filterNum)
{
    ArrayList result(filterNum);
    for(int i=0; i<filterNum;i++)
    {
        result.Data[i]=fftpowerlist.Dot(Trimf(freqlist, triFilterbankPrmlist.GetColumnArrayList(i)));
    }
    return  result;
}

ArrayList OLib::Trimf(ArrayList x, ArrayList prm)
{
    double* result=new double[x.Count()];
    double a=prm.Data[0];
    double b=prm.Data[1];
    double c=prm.Data[2];
    
    
    for(int i=0;i<x.Count();i++)
    {
        if(x.Data[i]<=a || c<x.Data[i])
        {
            result[i]=0;
        }
    }
    
    if(a!=b)
    {
        for (int i=0; i< x.Count(); i++)
        {
            if( a<x.Data[i] && x.Data[i]<b )
            {
                result[i]=(x.Data[i]-a)/(b-a);
            }
        }
    }
    
    if(b!=c)
    {
        for( int i=0;i<x.Count();i++)
        {
            if(b<x.Data[i] && x.Data[i]<c)
            {
                result[i]=(c-x.Data[i])/(c-b);
            }
        }
    }
    
    for( int i=0;i<x.Count();i++)
    {
        if(b==x.Data[i])
        {
            result[i]=1;
        }
    }
    
    
    return ArrayList(result, x.Count());
}

ArrayList OLib::GetMfcc(ArrayList filterFactorArray, int mfccNum, int filterNum)
{
    double* mfcc=new double[mfccNum];
    for(int i=0;i<mfccNum;i++)
    {
        ArrayList coef(filterNum);
        for(int j=0;j<filterNum;j++)
        {
            coef.Data[j]=cos((pi/filterNum)*(i+1)* (j+1-0.5));
        }
        
        mfcc[i]= coef.Dot(filterFactorArray);
    }
    
    return ArrayList(mfcc,mfccNum);
}

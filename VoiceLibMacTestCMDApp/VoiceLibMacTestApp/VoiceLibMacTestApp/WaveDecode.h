//
//  WaveDecode.h
//  VoiceA
//
//  Created by 姜珂 on 14-9-5.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface WaveDecode : NSObject
{
    int ChannelCount;
    int SimplePerSec;
    int BitsPerSimple;
}

@property(nonatomic) int StreamIndex;


-(void) OpenWaveFile:(NSData*)data;
@end

//
//  Generator.cpp
//  VoiceSum
//
//  Created by 姜珂 on 14-10-8.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#include "Generator.h"

ArrayList::ArrayList(int Size)
{
    Data=new double[Size];
    _count=Size;
}

ArrayList::ArrayList(double* array, int len)
{
    Data=array;
    _count=len;
}

int ArrayList::Count()
{
    return _count;
}

ArrayList::~ArrayList()
{
    //delete[] Data;
}

VectorList::VectorList(double** array, int len1, int len2)
{
    Data=array;
    Len1=len1;
    Len2=len2;
}

double ArrayList::Dot(ArrayList to)
{
    double result=0;
    for(int i=0; i<Count(); i++)
        result+=Data[i]*to.Data[i];
    return result;
}

void ArrayList::Print()
{
    for(int i=0; i<_count;i++)
    {
        printf("%d : %f \n",i, Data[i]);
    }
}

double ArrayList::SumValue()
{
    double va=0;
    for(int i=0; i<_count;i++)
    {
        va+=Data[i];
    }
    
    return va;
}

double VectorList::SumValue()
{
    double va=0;
    for(int i=0; i< Len1;i++)
    {
        for (int j=0;j<Len2; j++) {
            va+=Data[i][j];
        }
    }
    
    return va;
}

ArrayList VectorList::GetColumnArrayList(int ii)
{
    double* result=new double[Len1];
    
    for(int i=0;i<Len1; i++)
    {
        result[i]=Data[i][ii];
    }
    
    return ArrayList(result,Len1);
}
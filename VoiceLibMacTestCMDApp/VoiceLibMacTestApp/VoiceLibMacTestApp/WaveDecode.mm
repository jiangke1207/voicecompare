//
//  WaveDecode.m
//  VoiceA
//
//  Created by 姜珂 on 14-9-5.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#import "WaveDecode.h"

#include "VoiceHelperLibEntry.h"

@implementation WaveDecode
@synthesize StreamIndex;

-(void) OpenWaveFile:(NSData*)data;
{
    [self ParseWaveFile:data];
}

-(void) ParseWaveFile:(NSData*)data;
{
    StreamIndex=0;
    
    NSLog(@"ID: %@",[self CovertToNSString:data length:4]);
    NSLog(@"Size: %d",[self CovertToInt:data  length:4]);
    NSLog(@"Type: %@",[self CovertToNSString:data length:4]);
    
    NSLog(@"ID: %@",[self CovertToNSString:data  length:4]);
    int formatchunksize=[self CovertToInt:data length:4];
    NSLog(@"Size: %d",formatchunksize);
    NSLog(@"Format Tag: %d",[self CovertToInt:data  length:2]);
    ChannelCount=[self CovertToInt:data length:2];
    NSLog(@"Channels: %d",ChannelCount);
    SimplePerSec=[self CovertToInt:data  length:4];
    NSLog(@"SimplePerSec: %d", SimplePerSec);
    NSLog(@"AvgBytesPerSec: %d",[self CovertToInt:data  length:4]);
    NSLog(@"BlockAlign: %d",[self CovertToInt:data  length:2]);
    BitsPerSimple=[self CovertToInt:data  length:2];
    NSLog(@"BitsPerSimple: %d",BitsPerSimple);
    if(formatchunksize==18)
        NSLog(@"Option: %d",[self CovertToInt:data  length:2]);
    
    while (true) {
        NSString* nextpart=[self CovertToNSString:data length:4];
        if([nextpart isEqual: @"data"])
        {
            break;
        }
    }
    
    //NSLog(@"ID: %@",nextpart);
    [self CovertToInt:data  length:4];
    [self ReadVoiceDate:data];
}

-(NSString*) CovertToNSString:(NSData*)dates  length:(int)len
{
    Byte byteid[len];
    NSRange nr=NSMakeRange(StreamIndex, len);
    [dates getBytes:byteid range:nr];
    
    NSData* date=[[NSData alloc] initWithBytes:byteid length:len];
    
    StreamIndex+=len;
    return [[NSString alloc] initWithData:date encoding:NSUTF8StringEncoding];
}

-(int) CovertToInt:(NSData*)dates  length:(int)len
{
    Byte byteid[len];
    NSRange nr=NSMakeRange(StreamIndex, len);
    [dates getBytes:byteid range:nr];
    
    int iOutcome = 0;
    
    for ( int i =0; i<len ; i++) {
        Byte bLoop = byteid[i];
        iOutcome+= (bLoop & 0xFF) << (8 * i);
        
    }
    StreamIndex+=len;
    return iOutcome;
}

-(void) ReadVoiceDate:(NSData*)dates
{
    int persimple=ChannelCount *(BitsPerSimple/8);
    unsigned long simplecount= (dates.length-StreamIndex)/persimple;
    
    double* doublearraydata=new double[simplecount];
    Byte* bytes=(Byte*)[dates bytes];
    int index=0;
    
    double max=-100000;
    double min=100000;
    double vmax=-1;
    double vmin=1;
    
    for (int i=StreamIndex; i<dates.length; i=i+persimple) {
        int value=0;
        value= (bytes[i+1]<<8)+bytes[i];
        
        if(value>=32768)
        {
            value=-(65536-value);
        }
        doublearraydata[index]=(value/32768.0);
        
        if(value > max) max=value;
        if(value < min) min=value;
        if(doublearraydata[index]>vmax) vmax=doublearraydata[index];
        if(doublearraydata[index]<vmin) vmin=doublearraydata[index];
        
        index++;
        
        if(i<StreamIndex+100)
            printf("%d %d %d, %f \n",bytes[i+1],bytes[i],value,value/32768.0);
    }
    
    printf("max:%f , min:%f , vmax:%f  ,vmin:%f, count=%d \n", max,min, vmax, vmin, simplecount);
    
    VoiceLibEntry* entry=new VoiceLibEntry();
    
    entry->GetMFCC(doublearraydata, simplecount, SimplePerSec);
}
@end

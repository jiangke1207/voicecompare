//
//  VoiceHelperLibEntry.h
//  VoiceSum
//
//  Created by 姜珂 on 14-10-8.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#ifndef __VoiceSum__VoiceHelperLibEntry__
#define __VoiceSum__VoiceHelperLibEntry__

#include <stdio.h>
#include "Generator.h"

class VoiceLibEntry
{
public:
    ArrayList GetMFCC(double* input, int len, int FS);
};

#endif /* defined(__VoiceSum__VoiceHelperLibEntry__) */

//
//  Generator.h
//  VoiceSum
//
//  Created by 姜珂 on 14-10-8.
//  Copyright (c) 2014年 KeSoft. All rights reserved.
//

#ifndef __VoiceSum__Generator__
#define __VoiceSum__Generator__

#include <stdio.h>
typedef struct
{
    double real ;
    double image ;
} COMPLEX ;

typedef struct
{
    int start;
    int end;
}FrameRange;


class ArrayList
{
public:
    ArrayList(double* array, int len);
    ArrayList(int Size);
    int Count();
    double* Data;
    
    double SumValue();
    void Print();
    
    double Dot(ArrayList to);

    ~ArrayList();
    
private:
    int _count;
    
};

class VectorList
{
public:
    VectorList(double** array, int len1, int len2);
    double** Data;
    int Len1;
    int Len2;
    double SumValue();
    
    ArrayList GetColumnArrayList(int i);
};


#endif /* defined(__VoiceSum__Generator__) */
